<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Hus - Formularios</title>
  <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
  <!-- Google Font: Source Sans Pro -->
  {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> --}}
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
<style>
    #toast {
            position: fixed;
            z-index: 1;
            right: 2.5%;
            top: 5%;
            border: 1px solid rgba(0, 0, 0, .1);
            border-radius: .25rem;
            box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .1);
            max-width: 310px;
            width: 310px;
            opacity: 1;
        }
        .lockscreen-image>img {
            border-radius: 0% !important;
        }
        .div-panel{
            width: 50%;
            padding: 0.5em;
            border-radius: 0.5em;
            background: rgb(248, 247, 246);
        }
        #btn-admin
        {
          position: absolute;
          margin-top: -50px;
          background: #6899d6;
          padding: 0.5em;
          border-radius: 0.5em;
          color: white;
          box-shadow: inset 1px -1px 5px;
        }
        .noti-submit
        {
          padding: 0.5em;
          background: #7494cf73;
          justify-content: center;
          border-radius: 0.5em;display:none
        }
        @media screen and (max-width: 600px){
            .div-panel{  width: 95%;}
        }
    
</style>
</head>
<body class="hold-transition lockscreen" style="background: url({{ asset('img/hus3.jpg')}}) 50% fixed;
background-size: cover;">
@if(session()->has('mensaje'))
<div class="alert alert-warning alert-dismissible" id="toast">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-warning"></i> ALERTA! </h4>
    {{session('mensaje')}}
</div>
@endif
<!-- Automatic element centering -->
<div class="lockscreen-wrapper div-panel">
  <div>
    <a href="{{url('login')}}" id="btn-admin">Admin</a>
  </div>
  <div class="lockscreen-logo">
    <a><b>Formulario de capacitaciones </b> HUS</a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name">Digite el código del formulario</div>
  <br><br>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
     {{-- <img src="../../dist/img/user1-128x128.jpg" alt="User Image"> --}}
      <img src="{{asset('img/firma.svg') }}" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->
    
    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials" action="{{url('viewformcapacitacion')}}" method="POST">
        @csrf
      <div class="input-group">
        <input type="number" class="form-control" name="codigo" placeholder="Código">        
        <div class="input-group-append">
          <button type="submit" class="btn" id="btn-submit">
            <i class="fas fa-arrow-right text-muted"></i>
          </button>
        </div>
      </div>
      
    </form>
    
    <!-- /.lockscreen credentials -->

  </div>
  <div style="text-align: center">
    <span id="mensaje-submit" class="noti-submit">Cargando formulario..</span>
  </div>
  
  <!-- /.lockscreen-item -->
  <br><br>
  <div class="lockscreen-footer text-center">
    Copyright &copy; 2022 <b><a href="http://www.hus.gov.co" class="text-black">Hospital Universitario de Santander</a></b><br>
    All rights reserved
  </div>
</div>
<!-- /.center -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $(".alert").fadeOut(1500);
        }, 2000);
        $("#btn-submit").click(function() {
        $("#btn-submit").hide();
        $("#mensaje-submit").show();

        });
    });

</script>
</body>
</html>
