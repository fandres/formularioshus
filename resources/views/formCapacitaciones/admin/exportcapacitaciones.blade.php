@extends('layouts.admincapacitaciones')
@section('contenido')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h6 class="card-title">Reportes</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <form action="{{url('reportescapacitacion')}}" method="POST">
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Fecha</label>
                                    <input type="date" name="fecha" class="form-control">
                                </div>
                                <div class="col-md-3">
                                    <label>Servicio</label>
                                    <select name="servicio" id="" class="form-control">
                                        <option value=""></option>
                                        @foreach($servicios as $servicio)
                                            <option value="{{$servicio->id}}">{{$servicio->nombre_servicio}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Cargo</label>
                                    <select name="cargo" id="" class="form-control">
                                        <option value=""></option>
                                        @foreach($cargos as $cargo)
                                            <option value="{{$cargo->id}}">{{$cargo->nombre_cargo}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Empresas</label>
                                    <select name="empresa" id="" class="form-control">
                                        <option value=""></option>
                                        @foreach($empresas as $empresa)
                                            <option value="{{$empresa->id}}">{{$empresa->nombre_empresa}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success pull-right" style="margin-top: 1em;"><i
                                    class="fa fa-download"> Generar Excel</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection