@extends('formCapacitaciones.index')
@section('contenido')
@section('titulo', $cabecera->tema)
{{-- <div class=""> --}}
    <cite style="font-size: 12px">Campos que son obligatorios <span class="rojo faa-flash animated faa-slow asterisco">*</span></cite>
<div class="col-md-12">
    <div class="well well-sm">
        <fieldset>
            <div class="row centrar">
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"></span>
                    <div class="col-md-12">
                        <input type="hidden" id="id_cabecera" value="{{$cabecera->id}}">
                        <cite>Fecha<span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <input id="fecha_entrega" type="date"  class="form-control" value="{{date("Y-m-d")}}">
                    </div>
                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"></span>
                    <div class="col-md-12">
                        <cite>Tipo Documento <span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <select name="tipodocumento" id="tipodocumento" class="form-control">
                            <option value="CC">CC</option>
                            <option value="CE">CE</option>
                            <option value="TI">TI</option>
                            <option value="RC">RC</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-2 text-center"></span>
                    <div class="col-md-12">
                        <cite>Documento<span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <input id="documento" type="text" placeholder="Campo obligatorio *" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"></span>
                        <cite>Nombres<span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <input id="nombres" type="text" placeholder="Campo obligatorio *" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                        <cite>Apellidos<span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <input id="apellidos" type="text" placeholder="Campo obligatorio *" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                        <cite>Servicio<span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <select id="servicio" class="form-control select2bs4" style="width: 100%;">
                            <option value="" selected>Seleccione una opción</option>
                            @foreach($servicios as $servicio)
                            <option value="{{$servicio->id}}">{{$servicio->nombre_servicio}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                        <cite>Cargo<span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <select id="cargo" class="form-control select2bs4" style="width: 100%;">
                            <option disabled selected value="">Seleccione una opción</option>
                            @foreach($cargos as $cargos)
                            <option value="{{$cargos->id}}">{{$cargos->nombre_cargo}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4" id="div_input" style="display:none">
                    <div class="form-group" >
                        <label for="otro">Especifique cual:</label>
                        <input type="text"  id="id_input_servicio" style="border: 2px solid #eb9b6c;" disabled required>
                    </div>
                </div>                
                <div class="col-md-4">
                    <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                        <cite>Empresa<span class="rojo faa-flash animated faa-slow">*</span></cite>
                        <select id="empresa" class="form-control select2bs4" style="width: 100%;">
                            <option disabled selected value="">Seleccione una opción</option>
                            @foreach($empresas as $empresa)
                            <option value="{{$empresa->id}}">{{$empresa->nombre_empresa}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <center>
                <h4><i class="fas fa-file-signature"></i><span class="rojo faa-flash animated faa-slow">*</span></h4>
                <p>Firma:</p>
                <div class="">
                    <div class="wrapper">
                        <canvas id="firmaEntrega" class="firmaEntrega"  width="450px"
                        height="200px" style="border-top: 1px solid #ced4da;
                          border-right: 1px solid #ced4da;
                          border-bottom: 1px solid #ced4da;
                          border-left: 1px solid #ced4da;"></canvas>
                    </div>
                </div>
                <div class="col-sm-4">
                    <button class="btn btn-secondary" id="clear">Limpiar</button>
                </div>
            </center>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <footer style="padding-bottom: 1em;padding-top: 1em;">
                        <button class="btn btn-primary btn-lg btn-block fondocolor" id="save">Guardar</button>
                        <button class="btn btn-warning btn-lg btn-block" style="display: none" id="guardando">Guardando información ... <i class="fas fa-spinner fa-pulse"></i></button>
                    </footer>
                </div>
            </div>
            @if($cabecera->linkevaluacion)
            <div class="row pt-2 pb-2">
                <div class="col-md-6">
                    <a href="{{$cabecera->linkevaluacion}}" target="black">Link de la Evaluación</a>
                </div>
            </div>
            @endif
            {{-- <div class="alert alert-success" style="display:none"></div> --}}
            <div class="alert alertTime alert-success alert-dimdissible" style="display: none;z-index:100" id="toast">
                <i class="fas fa-check-circle"></i>
            </div>
            <div class="error alertTime alert-danger alert-dimdissible" style="display: none;" id="toast">
                <i class="fas fa-exclamation-triangle  faa-wrench animated faa-fast"></i>
                <p id="mensajeError"></p>
            </div>

        </fieldset>
    </div>
    <details>
        <summary><strong>Copyright &copy; 2021 <a href="http://www.hus.gov.co/">Hospital Universitario de
                    Santander</a></strong>. All rights reserved.</summary>
        <p> - Desarrollado por: Ing.Fabián Andrés Durán Santos.</p>
    </details>
</div>
@endsection
{{-- </div>
        <!--1 row--> --}}
@section('scriptentrega')
<script>
    $(function () {
    $('.select2bs4').select2({
      theme: 'bootstrap4'
        });
    });
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        // $('#documento').val('1100');
        // $('#nombres').val('1100');
        // $('#cargo').val('1');
        // $('#servicio').val('1');
        // $('#empresa').val('UIS');

        $("#servicio").change( function() {
            if ($(this).val() === "99") {
                $("#div_input").show("fast");
                $("#id_input_servicio").prop("disabled", false);
               
            } else {
                $("#div_input").hide("fast");
                $("#id_input_servicio").prop("disabled", true);
                
            }
        });

        var canvas = document.getElementById('firmaEntrega');
        var signaturePad = new SignaturePad(canvas, {});

        var saveButton = document.getElementById('save');
        var clearButton = document.getElementById('clear');
        saveButton.addEventListener('click', function(event) {
            var firmaEntrega = signaturePad.toDataURL('image/png');

            // var firmavacunador = signaturePadVacunador.toDataURL('image/png');
            //console.log(signaturePadAcudiente.toDataURL('image/png'));
            //if (signaturePad.isEmpty() || signaturePadVacunador.isEmpty()) {
            if (signaturePad.isEmpty()) {
                // alert("Firma Obligatoria");
                $(".alert-danger").show();
                $(".alert-danger").fadeOut(5500);
                $("#mensajeError").text('Firma Obligatoria');

                //$("#mensajeError").text('Texto de sustitución');
            } else {

                var tipodocumento = $('#tipodocumento').val();
                var fecha_entrega = $('#fecha_entrega').val();
                var documento = $('#documento').val();
                var nombres = $('#nombres').val();
                var apellidos = $('#apellidos').val();
                var cargo = $('#cargo').val();
                var id_cabecera = $('#id_cabecera').val();
                if($('#servicio').val() == "99")
                {
                    var servicio = $('#id_input_servicio').val();
                }
                else
                {
                    var servicio = $('#servicio').val();
                }
                var empresa = $('#empresa').val();
     
                    $('#save').hide();
                    $('#guardando').fadeIn();
                    $.ajax({
                        url: "{{ url('/capacitacion') }}"
                        , method: 'post'
                        , data: {
                            signature: firmaEntrega
                            , nombres: nombres
                            , apellidos: apellidos
                            , tipodocumento: tipodocumento
                            , documento: documento
                            , cargo: cargo
                            , servicio: servicio
                            , empresa: empresa
                            , fecha_entrega: fecha_entrega
                            , id_cabecera: id_cabecera
                        , }
                        , success: function(result) {
                            console.log(result);
                            jQuery('.alert').show();
                            jQuery('.alert').html(result.success);
                            $('#nombres').val('');
                            $('#apellidos').val('');
                            $('#cargo').select2('val', 'All');
                            $('#servicio').select2('val', 'All');
                            $('#documento').val('');
                            $('#empresa').select2('val', 'All');                            
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                            signaturePad.clear();
                            // signaturePadVacunador.clear();
                            setTimeout(function() {
                                $(".alertTime").fadeOut(1500);
                            }, 2000);
                            $('html, body').animate({
                                scrollTop: 0
                            }, 'slow');
                        }
                        , error: function(msj) {

                            console.log(msj.responseJSON);
                            console.log(msj);
                            //console.log(msj.responseJSON.apellidos[0]);
                            console.log('hola fabian');
                            //console.log(msj.errors.apellidos);
                            $('.error').show();
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                            $("#mensajeError").text('Existen campos sin diligenciar');
                            setTimeout(function() {
                                $(".alertTime").fadeOut(100);
                            }, 1500);
                        }
                    }); //respuesta
                
            } // cierre else
        }); //evento boton
        clearButton.addEventListener('click', function() {
            signaturePad.clear();
        });
    });

</script>

@endsection
