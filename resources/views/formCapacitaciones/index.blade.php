<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="_token" content="{{csrf_token()}}" />
    <title>Formulario</title>
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('js/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/awesome-animation.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
    {{--
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/font-awesome-animation@1.1.1/css/font-awesome-animation.css"> --}}
    <style>
        #toast {
            position: fixed;
            z-index: 1;
            right: 2.5%;
            top: 15%;
            border: 1px solid rgba(0, 0, 0, .1);
            border-radius: .25rem;
            box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .1);
            max-width: 310px;
            width: 310px;
            opacity: 1;
            padding: 0.5em;
            height: 80px;
        }

        .fondocolor {
            /* background: rgb(22, 170, 231); */
            background: linear-gradient(166deg, rgb(22 170 231 / 50%) 30%, rgb(0 92 161) 50%, rgb(62 163 189) 100%);
        }

        .txtblanco {
            color: whitesmoke
        }

        table,
        th,
        td {
            border: 1px solid #dee2e6;
            border-collapse: collapse;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 720px !important;
            }
        }

        .rojo {
            color: red;
            font-size: 1.2em;
        }

        .asterisco {
            position: absolute;
            font-size: 3em;
            margin: -8px -5px 5px 5px;
        }

        .img {
            width: 30%;
            text-align: center;
            margin: -5% 50px 30px auto;
        }

        .centrar {
            justify-content: center !important;
        }

        .btnaccion,
        .btn1,
        .btn2,
        .btn3 {
            vertical-align: top;
            display: inline;
        }

        .btnaccion {
            width: 100%;
            position: absolute !important;
            margin: -2em -10em 0.5em 3em !important;
        }

        @media (min-width: 1200px) {
            .btnaccion {

                margin: -2em -10em 0.5em 3em !important;
            }
        }

        @media only screen and (max-width: 773px) {
            .btnaccion {

                /* width: 0%; */
                margin: -3em -10em 0.5em 1em !important;
            }

            .btn1 {
                width: 0% !important;
            }
        }

        @media only screen and (max-width: 600px) {
            .btnaccion {
                margin: -1em -10em 0.5em -1em !important;
            }
        }
    </style>
</head>

<body style="background:#6c757d30">
    <div class="container" style="border-width: 2px;
    border-style: solid;
    border-color: #00000033;
    width:95% !important;
    box-shadow: 0px 0px 10px 2px #343a40;background:white;">
        <div class="jumbotron fondocolor txtblanco">
            <img src="{{asset('banner.png')}}" class="img" alt="">

            <h4 class="display-6 text-center">@yield('titulo')</h4>
        </div>
        @yield('contenido')
    </div>
    <!-- Link to js file in folder app and CDN used for the signature pad :  Jquery, ajax, signature pad -->
    <script src="{{ url('js/app.js') }} " charset="utf-8"></script>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/signature.js')}}"></script>
    <script src="{{ asset('adminlte/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('adminlte/js/adminlte.min.js') }}"></script>

    <!-- AJAX to save signature pad content -->
    @yield('scriptentrega')
    @yield('scriptdevoluciones')
    
</body>

</html>