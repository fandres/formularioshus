@extends('layouts.adminFormCarnet')
@section('contenido')
<h5>Generar reporte</h5>
<div class="row">
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">POR FECHA</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <form action="{{ url('excelfecha') }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Fecha Inicial:</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" name="inicio" required>
                                </div>
                            </div>
                            <div class=" form-group">
                                <label class="col-sm-12 control-label">Fecha Final:</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" name="final" required>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success pull-left" style="margin-right: 5px;"><i
                                class="fa fa-download"> Generar Excel</i></button>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col (left) -->
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">POR INSTITUCIONES</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ url('excelinstitucion') }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Fecha Inicial:</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" name="inicio" required>
                                </div>
                            </div>
                            <div class=" form-group">
                                <label class="col-sm-12 control-label">Fecha Final:</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" name="final" required>
                                </div>
                            </div>
                            <div class=" form-group">
                                <label class="col-sm-12 control-label">Institución:</label>
                                <div class="col-sm-12">
                                    <select name="institucion" class="form-control">
                                        <option disabled selected value="">Seleccione una opción</option>
                                        @foreach($instituciones as $institucione)
                                        <option value="{{$institucione->id}}">{{$institucione->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success pull-left" style="margin-right: 5px;"><i
                                class="fa fa-download"> Generar Excel</i></button>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col (right) -->
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">POR PROGRAMA</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ url('excelprograma') }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Fecha Inicial:</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" name="inicio" required>
                                </div>
                            </div>
                            <div class=" form-group">
                                <label class="col-sm-12 control-label">Fecha Final:</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" name="final" required>
                                </div>
                            </div>
                            <div class=" form-group">
                                <label class="col-sm-12 control-label">Programa:</label>
                                <div class="col-sm-12">
                                    <select id="programa" name="programa" class="form-control">
                                        <option disabled selected value="">Seleccione una opción</option>
                                        @foreach($programas as $programa)
                                        <option value="{{$programa->id}}">{{$programa->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success pull-left" style="margin-right: 5px;"><i
                                class="fa fa-download"> Generar Excel</i></button>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection