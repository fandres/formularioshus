<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="_token" content="{{csrf_token()}}" />
    <title>Ficha Formulario</title>
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .imgfirma {
            width: 85%;
            position: relative;
        }
        .row label{
            font-family: cursive;
        }
    </style>
</head>

<body style="background:#6c757d30">
    <div class="container" style="border-width: 2px;
    border-style: solid;
    border-color: #00000033;
    box-shadow: 0px 0px 10px 2px #343a40;background:white;margin-top:2em;padding:1em">
        <div class="col-md-12">
            <div class="well well-sm centrar">
                <fieldset>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col col-sm-6 col-md-3">
                                    <label>Nombres</label>
                                    <p>{{$ficha->nombres}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>Apellidos</label>
                                    <p>{{$ficha->apellidos}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>Documento</label>
                                    <p>{{$ficha->documento}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>perfil</label>
                                    <p>{{$ficha->perfil}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-6 col-md-3">
                                    <label>Programa</label>
                                    <p>{{$ficha->programa}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>Institución</label>
                                    <p>{{$ficha->institucion}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>Fecha Entrega</label>
                                    <p>{{$ficha->fecha_entrega}}</p>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col col-sm-6 col-md-3">
                                    <label>Carnets</label>
                                    <p>{{$ficha->Carnet}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>Cintas</label>
                                    <p>{{$ficha->Cinta}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>Porta carnet</label>
                                    <p>{{$ficha->Porta_carnet}}</p>
                                </div>
                            </div> 
                            @if($ficha->fecha_devolucion)
                            <div class="row">
                                <div class="col col-sm-6 col-md-3">
                                    <label>Fecha devolución</label>
                                    <p>{{$ficha->fecha_devolucion}}</p>
                                </div>
                            </div>
                            @endif
                            @if($ficha->fecha_reimpresion)
                            <div class="row">
                                <div class="col col-sm-6 col-md-3">
                                    <label>Fecha Reimpresion</label>
                                    <p>{{$ficha->fecha_reimpresion}}</p>
                                </div>
                                <div class="col col-sm-6 col-md-3">
                                    <label>Observación</label>
                                    <p>{{$ficha->observacion}}</p>
                                </div>
                            </div>
                            <div class="rpw">
                                <div class="row">
                                    <div class="col col-sm-6 col-md-3">
                                        <label>Carnets</label>
                                        <p>{{$ficha->reimpresioncarnet}}</p>
                                    </div>
                                    <div class="col col-sm-6 col-md-3">
                                        <label>Cintas</label>
                                        <p>{{$ficha->reimpresioncinta}}</p>
                                    </div>
                                    <div class="col col-sm-6 col-md-3">
                                        <label>Porta carnet</label>
                                        <p>{{$ficha->reimpresionportacarnet}}</p>
                                    </div>
                                </div> 
                            </div>
                            @endif                        
                        </div>
                        <div class="col col-sm-6 col-md-4">
                            <label>Firma Entrega</label>
                            <img class="imgfirma" src="data:image/bmp;base64,{{$ficha->firmaEntrega}}" alt="">
                            @if($ficha->firmaReimpresion)
                            <label>Firma Reimpresión</label>
                            <img class="imgfirma" src="data:image/bmp;base64,{{$ficha->firmaReimpresion}}" alt="">
                            @endif
                            @if($ficha->firmaDevolucion)
                            <label>Firma devolución</label>
                            <img class="imgfirma" src="data:image/bmp;base64,{{$ficha->firmaDevolucion}}" alt="">
                            @endif
                        </div>                        
                    </div>
                    <hr>
                    <div class="row">
                        <details>
                            <summary><strong>Copyright &copy; 2021 <a href="http://www.hus.gov.co/">Hospital Universitario de
                                        Santander</a></strong>. All rights reserved.</summary>
                            <p> - Desarrollado por: Ing.Fabián Andrés Durán Santos.</p>
                        </details>
                    </div>
                    
                </fieldset>
            </div>
        </div>
    </div>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
</body>

</html>