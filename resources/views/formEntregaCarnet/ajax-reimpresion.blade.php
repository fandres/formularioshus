@extends('formEntregaCarnet.index')
@section('contenido')
@section('titulo', 'REIMPRESIÓN DE CARNET INSTITUCIONAL A ESTUDIANTES Y DOCENTES')
{{-- <div class=""> --}}
    <div class="col-md-12">
        <div class="well well-sm">
            <fieldset>
                <div class="row pb-3">
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="documento" placeholder="Documento" required />
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary" onclick="consultarEntregas()">
                            Consultar
                        </button>
                    </div>
                </div>
                <div class="card-header">
                    <h5 class="card-title">
                        <i class="fas fa-text-width"></i>
                        Reimpresión
                    </h5>
                </div>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Notificación!</h5>
                    <b>Fecha reimpresion: </b><span id="rrimpresion"></span> 
                    <b>Cinta: </b><span  id="recinta"></span> 
                    <b>Carnet: </b><span id="recarnet"></span>
                    <b>Porta Carnet: </b><span id="reporta"></span>
                  </div>
                <div id="id_row_pendientes" style="display: none">
                    <div class="row pb-3">
                        <div class="col-md-3">
                            <b>Fecha entrega:</b>
                            <span id="fecha_entrega"></span>
                        </div>
                        <div class="col-md-4">
                            <b>Fecha Reimpresión</b>
                            <input type="date" id="fecha_reimpresion" value="{{date("Y-m-d")}}">
                            <span class="rojo faa-flash animated faa-slow">*</span>
                        </div>
                        <div class="col-md-3">
                            <b>Nombres:</b>
                            <br>
                            <span for="" id="nombres"></span>
                            <span for="" id="apellidos"></span>
                        </div>
                        <input type="hidden" id="id_entrega">
                    </div>
                    <div class="row">
                        <textarea id="observacion" cols="100%" rows="6" placeholder="Obaservación"></textarea>
                    </div>
                    <!--row-->
                    <cite>Elementos a Reimprimir:</cite><span class="rojo faa-flash animated faa-slow">*</span>
                    <input id="carnetEntrega" type="hidden" class="form-control">
                    <input id="cintaEntrega" type="hidden" class="form-control">
                    <input id="portacarnetEntrega" type="hidden" class="form-control">
                    <div class="row pb-3" style="justify-content: center">
                        <div class="col-md-2" id="bcarnet" style="display: none">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Carnet</cite>
                                <input id="carnet" type="checkbox" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2" id="bcinta" style="display: none">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Cinta</cite>
                                <input id="cinta" type="checkbox" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3" id="bportacarnet" style="display: none">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>porta Carnet</cite>
                                <input id="portacarnet" type="checkbox" class="form-control">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <center>
                        <h4><i class="fas fa-file-signature"></i><span class="rojo faa-flash animated faa-slow">*</span>
                        </h4>
                        <p>Firma:</p>
                        <div class="">
                            <div class="wrapper">
                                <canvas id="firmaReimpresion" class="firmaReimpresion" width=350 height=200 style="border-top: 1px solid #ced4da;
                          border-right: 1px solid #ced4da;
                          border-bottom: 1px solid #ced4da;
                          border-left: 1px solid #ced4da;"></canvas>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-secondary" id="clear">Limpiar</button>
                        </div>
                    </center>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <footer style="padding-bottom: 1em;padding-top: 1em;">
                                <button class="btn btn-primary btn-lg btn-block fondocolor" id="save">Guardar</button>
                                <button class="btn btn-warning btn-lg btn-block" style="display: none"
                                    id="guardando">Guardando
                                    información ... <i class="fas fa-spinner fa-pulse"></i></button>
                            </footer>
                        </div>
                    </div>
                </div>
                {{-- <div class="alert alert-success" style="display:none"></div> --}}
                <div class="alert alertTime alert-success alert-dimdissible" style="display: none;z-index:100"
                    id="toast">
                    <i class="fas fa-check-circle"></i>
                </div>
                <div class="error alertTime alert-danger alert-dimdissible" style="display: none;" id="toast">
                    <i class="fas fa-exclamation-triangle  faa-wrench animated faa-fast"></i>
                    <p id="mensajeError"></p>
                </div>

            </fieldset>
        </div>
    </div>
    @endsection
    {{--
</div>
<!--1 row--> --}}
@section('scriptentrega')
<script>
    function  consultarEntregas() {
        let documento = $('#documento').val();
            $.ajax({
                url: "{{ url('/consultadevolver') }}"
                , method: 'post'
                , data: {
                    documento: documento,
                 }
                , success: function(result) {
                    console.log(result);
                    if(result == 1)
                    {
                        $(".alert-danger").show();
                        $(".alert-danger").fadeOut(5500);
                        $("#mensajeError").text('No se encuentra nada pendiente');
                    }
                    else
                    { 
                        let carnetEntrega = $('#carnetEntrega').val(result.carnet);
                        let cintaEntrega  = $('#cintaEntrega').val(result.cinta);
                        let portacarnetEntrega  = $('#portacarnetEntrega').val(result.portacarnet);
                        $('#carnet').prop('checked', false);
                        $('#cinta').prop('checked', false);
                        $('#portacarnet').prop('checked', false);
                        $('#id_row_pendientes').fadeIn();
                        $('#nombres').text(result.nombres);
                        $('#apellidos').text(result.apellidos);
                        $('#fecha_entrega').text(result.fecha_entrega);
                        $('#id_entrega').val(result.id);
                        $('#carnet').val(result.carnet);
                        $('#cinta').val(result.cinta);
                        $('#portacarnet').val(result.portacarnet);

                        $('#recarnet').text(result.recarnet);
                        $('#recinta').text(result.recinta);
                        $('#reporta').text(result.reportacarnet);
                        $('#rrimpresion').text(result.rfechareimpresion);
                        console.log(result.rfechareimpresion);

                        $('#bcarnet').hide();
                        $('#bcinta').hide();
                        $('#bportacarnet').hide();
                        if(result.carnet ==1)
                        {
                            $('#bcarnet').show();
                            $('#carnet').prop('checked', true);
                        }
                        if(result.cinta ==1)
                        {
                            $('#bcinta').show();
                            $('#cinta').prop('checked', true);
                        }
                        if(result.portacarnet ==1)
                        {
                            $('#bportacarnet').show();
                            $('#portacarnet').prop('checked', true);
                        }
                    }//else
                }
            }); //respuesta

        }; //evento boton
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        var canvas = document.getElementById('firmaReimpresion');
        var signaturePad = new SignaturePad(canvas, {});

        var saveButton = document.getElementById('save');
        var clearButton = document.getElementById('clear');
        saveButton.addEventListener('click', function(event) {
            var firmaReimpresion = signaturePad.toDataURL('image/png');
            if (signaturePad.isEmpty()) {
                // alert("Firma Obligatoria");
                $(".alert-danger").show();
                $(".alert-danger").fadeOut(5500);
                $("#mensajeError").text('Firma Obligatoria');
                //$("#mensajeError").text('Texto de sustitución');
            } else {

                var fecha_reimpresion = $('#fecha_reimpresion').val();
                var carnet = $('#carnet:checked').val();
                var cinta = $('#cinta:checked').val();
                var portacarnet = $('#portacarnet:checked').val();
                var observacion = $('#observacion').val();
               // let portacarnet  = $('#portacarnet').val();
                var id_entrega = $('#id_entrega').val();
                let carnetEntrega = $('#carnetEntrega').val();
                let cintaEntrega  = $('#cintaEntrega').val();
                let portacarnetEntrega  = $('#portacarnetEntrega').val();
                

                if ($('#carnet').is(':checked') || $('#cinta').is(':checked')|| $('#portacarnet').is(':checked')) {
                    console.log('entro a los or');
                    $('#save').hide();
                    $('#guardando').fadeIn();
                    $.ajax({
                        url: "{{ url('/reimprimiendoelementos') }}"
                        , method: 'post'
                        , data: {
                            signature: firmaReimpresion
                            , form_entrega_carnets: id_entrega
                            , fecha_reimpresion: fecha_reimpresion
                            , carnet: carnet
                            , cinta: cinta
                            , portacarnet: portacarnet
                            , carnetEntrega : carnetEntrega
                            , cintaEntrega : cintaEntrega
                            , portacarnetEntrega : portacarnetEntrega
                            , observacion
                        , }
                        , success: function(result) {
                            console.log('no sirbvw');
                            console.log(result);
                            jQuery('.alert').show();
                            jQuery('.alert').html(result.success);
                            $('#documento').val(''); 
                            $('#id_row_pendientes').fadeOut();                    
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                            signaturePad.clear();
                            // signaturePadVacunador.clear();
                            setTimeout(function() {
                                $(".alertTime").fadeOut(1500);
                            }, 2000);
                            $('html, body').animate({
                                scrollTop: 0
                            }, 'slow');
                        }
                        , error: function(msj) {
                            console.log(msj.responseJSON);
                            console.log(msj);
                            //console.log(msj.responseJSON.apellidos[0]);
                            console.log('hola fabian');
                            //console.log(msj.errors.apellidos);
                            $('.error').show();
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                            $("#mensajeError").text('Existen campos sin diligenciar');
                            setTimeout(function() {
                                $(".alertTime").fadeOut(100);
                            }, 1500);
                        }
                    }); //respuesta
                } // cierre if valida checkboc
                else {
                    $(".alert-danger").show();
                    $(".alert-danger").hide(4500);
                    $("#mensajeError").text('Seleccione: Carnet o Cinta');
                }
            } // cierre else
        }); //evento boton
        clearButton.addEventListener('click', function() {
            signaturePad.clear();
        });
    });

</script>
@endsection