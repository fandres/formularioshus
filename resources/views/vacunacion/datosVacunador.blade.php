@extends('layouts.admin')
@section('content')
 <!--DATOS DEL ACUDIENTE-->
 <div class="row">
    <div class="col-sm-12">
        <p>Identificación y firma del vacunador</p>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <span class="col-md-1 col-md-offset-2 text-center"></span>
            <cite>Nombre Vacunador<span class="rojo faa-flash animated faa-slow">*</span></cite>
            <input id="nombres_vacunador" type="text" placeholder="Campo obligatorio *" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <span class="col-md-1 col-md-offset-2 text-center"></i></span>
            <cite>Apellidos Vacunador<span class="rojo faa-flash animated faa-slow">*</span></cite>
            <input id="apellidos_vacunador" type="text" placeholder="Campo obligatorio *" class="form-control">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <span class="col-md-1 col-md-offset-2 text-center"></span>
            <cite>Tipo de documento<span class="rojo faa-flash animated faa-slow">*</span></cite>
            <select name="tipocedula_vacunador" id="tipocedula_vacunador" class="form-control">
                <option value=""></option>
                <option value="CC">CC</option>
                <option value="CE">CE</option>
                <option value="TI">TI</option>
                <option value="RC">RC</option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <span class="col-md-1 col-md-offset-2 text-center"></span>
            <cite>Documento<span class="rojo faa-flash animated faa-slow">*</span></cite>
            <input id="cedula_vacunador" type="text" placeholder="Campo obligatorio *" class="form-control">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <span class="col-md-1 col-md-offset-1 text-center"></span>
            <cite>Lugar Expedición<span class="rojo faa-flash animated faa-slow">*</span></cite>
            <input id="lugar_expecedula_vacunador" type="text" placeholder="Campo obligatorio *" class="form-control">
        </div>
    </div>
</div>
<center>
    <h4><i class="fas fa-file-signature"></i><span class="rojo faa-flash animated faa-slow">*</span></h4>
    <p>Firma:</p>
    <div class="">
        <div class="wrapper">
            <canvas id="firma_vacunador" class="firma_empleado" width=450 height=200 style="border-top: 1px solid #ced4da;
      border-right: 1px solid #ced4da;
      border-bottom: 1px solid #ced4da;
      border-left: 1px solid #ced4da;"></canvas>
        </div>
    </div>
    <div class="col-sm-4">
        <button class="btn btn-secondary" id="clearVacunador">Limpiar Firma</button>
    </div>
</center>
<div class="row">
    <div class="col-sm-12">
        <footer style="padding-bottom: 1em;padding-top: 1em;">
            <button class="btn btn-primary btn-lg btn-block fondocolor" id="save">Guardar</button>
            <button class="btn btn-warning btn-lg btn-block" style="display: none" id="guardando">Guardando información ... <i class="fas fa-spinner fa-pulse"></i></button>
        </footer>
    </div>
</div>
{{-- <div class="alert alert-success" style="display:none"></div> --}}
<div class="alert alertTime alert-success alert-dimdissible" style="display: none;z-index:100" id="toast">
    <i class="fas fa-check-circle"></i>
</div>
<div class="error alertTime alert-danger alert-dimdissible" style="display: none" id="toast">
    <i class="fas fa-exclamation-triangle  faa-wrench animated faa-fast"></i> Existen Campos vacios
</div>
@endsection