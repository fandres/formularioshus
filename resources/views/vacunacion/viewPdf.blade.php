<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <style>
        body {
            font-size: 12px;
        }

        table,
        th {
            border: 1px solid black;
            border-collapse: collapse;
            width: 100%;
        }

        .thInicial {
            border: 1px solid rgb(255, 255, 255);
            border-collapse: collapse;
        }

        table,
        th,
        td {
            padding: 0.5em;
        }

        td {
            width: 100%
        }

        .tdLinea {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .tddatos {
            width: 25px !important;
        }

        .subrayado {
            text-decoration: underline;
        }

        img {
            z-index: 1000;
            width: 150px;
            position: relative;
            margin: 10px 15px -50px 30px;
            padding-top: 1em;
        }

    </style>
</head>
<body class="text-justify">
    <h4 class="text-center">CONSENTIMIENTO INFORMADO PARA LA APLICACIÓN DE LA VACUNA CONTRA EL SARS-CoV2/COVID-19</h4>

    <table>
        <tr>
            <th>CIUDAD: {{ $pdf->ciudad}}</th>
            <th>FECHA: {{ \Carbon\Carbon::parse($pdf->fecha_consentimiento)->format('Y-m-d')}}</th>
            <th>HORA: {{ \Carbon\Carbon::parse($pdf->registro)->format('H:i')}} </th>
        </tr>
    </table>
    <table class="">
        <tbody>
            <tr>
                <th colspan="3" class="text-center">DATOS DE IDENTIFICACIÓN DE LA PERSONA</th>
            </tr>
            <tr>
                <td><b>NOMBRES:</b> {{$pdf->nombres}}</td>
                <td colspan="2"><b>APELLIDOS:</b> {{$pdf->apellidos}}</td>
            </tr>
            <tr>
                <td><b>TIPO DE IDENTIFICACIÓN:</b> {{$pdf->tipocedula}}</td>
                <td><b>NÚMERO:</b> {{$pdf->cedula}}</td>
                <td></td>
            </tr>
            <tr>
                <td><b>FECHA NACIMIENTO: </b>{{$pdf->fecha_nacimiento}} </td>
                <td><b>EDAD: </b>{{$pdf->edad}} </td> 
                <td></td>           
            </tr>
            <tr>
                <td colspan="3"><b>NOMBRE DE LA EAPB RESPONSABLE:</b> {{$pdf->eps}}</td>
                
            </tr>
        </tbody>
    </table>
    <table>
        <tr>
            <th colspan="3" class="text-center">INFORMACIÓN PREVIA</th>
        </tr>
        <tr>
            <td colspan="3" class="text-justify">@include('vacunacion._informacionPrevia')</td>
        </tr>
    </table>
    <table class="text-justify">
        <tr>
            <th colspan="3" class="text-center">Datos importantes de la vacunacion (Por favor, leer con detenimiento. Puede formular preguntas)</th>

        </tr>
        <tr>
            <td class="tddatos tdLinea">Vacuna y Dosis</td>
            <td colspan="2" class="tdLinea">Nombre de la Vacuna aplicar: <b class="subrayado">{{$pdf->nombre_vacuna}}</b> Esquema que tiene esta vacuna:
                @switch($pdf->dosis)
                @case(1)
                Dos Dosis <b>_</b> o dosis única <b class="subrayado">X</b>
                @break
                @case(2)
                Dos Dosis <b class="subrayado">X</b> o dosis única <b>_</b>
                @break
                @default

                @endswitch
            </td>
        </tr>
        <tr>
            <td class="tddatos">¿Comó se aplica</td>
            <td colspan="2" class="tdLinea">Administración vía intramuscular en el brazo (tercio medio del musculo deltoides)</td>
        </tr>
        <tr>
            <td class="tddatos tdLinea">Beneficios</td>
            <td colspan="2" class="tdLinea">Prevención de la enfermedad COVID-19, causada por el virus SARS-CoV-2. Reducción de la severidad de la enfermedad en caso de presentarse.
                Potencial protección del entorno familiar y los allegados.
            </td>
        </tr>
        <tr>
            <td class="tddatos tdLinea">Riesgos</td>
            <td colspan="2" class="tdLinea">Presentación de efectos adversos a corto y mediano plazo posterior a su aplicación como: dolor en el sitio de inyección, dolor de cabeza (cefalea, articulaciones (artralgia) , muscular (mialgia); fatiga (cansancio ); resfriado; fiebre (pirexias;)enrojecimiento e inflamación leve en el lugar de la inyección; inflamación de los ganglios (linfadenopatía); malestar general; sensación de adormecimiento en las extremidades, reacciones alérgicas leves, moderadas o severas.
                Estos no se presentan en todas las personas.
            </td>
        </tr>
        <tr>
            <td class="tddatos tdLinea" style="width: 100%">Alternativas</td>
            <td colspan="2" class="tdLinea">A la fecha no se ha identificado otra medida farmacológica más eficaz que la vacunación para la prevención de la COVID-19.</td>
        </tr>
    </table>
    <div style="page-break-after:always;"></div>
    <table>
        <tr>
            <th class="text-center">EXPRESIÓN DE LA VOLUNTAD</th>
        </tr>
        <tr>
            <td class="text-justify tdLinea">@include('vacunacion._expresionVolunta')</td>
        </tr>
        <tr>
            <td class="tdLinea">En consecuencia, decido
                @switch($pdf->acepta)
                @case('SI')
                <b>ACEPTAR:</b><b class="subrayado"> X</b> que se me aplique la vacuna. <b>NO ACEPTAR:</b>__ que se me aplique la vacuna.
                @break
                @case('NO')
                <b>ACEPTAR:</b>__ que se me aplique la vacuna. <b>NO ACEPTAR:</b><b class="subrayado"> X</b> que se me aplique la vacuna.
                @break
                @default

                @endswitch

                @if(is_null($pdf->firma_empleado))
                <p style="padding-top: 2.5em"><b>Firma: ________________________</b></p>
                @else
                <img src="{{asset('upload/'.$pdf->firma_empleado)}}" alt="">
                <p class="firmatop"><b>Firma: ________________________</b></p>
                @endif
            </td>
        </tr>
        <tr>
            <td><b>Razón por la que no firma:</b> {{$pdf->razon_nofirma}}</td>
        </tr>
    </table>
    <!--DATOS ACUDIENTE-->
    <table>
        <tr>
            <td colspan="3">En caso de requerirse, identificación y firma de quien tiene la patria potestad, la representación legal o la custoda:</td>
        </tr>
        <tr>
            <td colspan="2"><b>Nombres:</b> {{$pdf->nombres_acudiente}}</td>
            <td><b>Apellidos:</b> {{$pdf->apellidos_acudiente}}</td>
        </tr>
        <tr>
            <td><b>Tipo Identificación:</b> {{$pdf->tipocedula_acudiente}}</td>
            <td colspan="2"><b>Número:</b> @if(!is_null($pdf->cedula_acudiente)) {{$pdf->cedula_acudiente}} <b>de</b> {{$pdf->lugar_expecedula_acudiente}} @endif</td>
        </tr>
        <tr>
            <td colspan="3">
                @if(is_null($pdf->firma_acudiente))
                <p style="padding-top: 2.5em"><b>Firma: ________________________</b></p>
                @else
                <img src="{{asset('upload/'.$pdf->firma_acudiente)}}" alt="">
                <p><b>Firma: ________________________</b></p>
                @endif
            </td>
        </tr>
    </table>
    <!--DATOS VACUNADOR-->
    <table>
        <tr>
            <td colspan="3">Identificación y firma del vacunador:</td>
        </tr>
        <tr>
            <td colspan="2"><b>Nombres:</b> {{$pdf->nombres_vacunador}}</td>
            <td><b>Apellidos:</b> {{$pdf->apellidos_vacunador}}</td>
        </tr>
        <tr>
            <td><b>Tipo Identificación:</b> {{$pdf->tipocedula_vacunador}}</td>
            <td colspan="2"><b>Número:</b> @if(!is_null($pdf->cedula_vacunador)) {{$pdf->cedula_vacunador}} <b>de</b> {{$pdf->lugar_expecedula_vacunador}} @endif </td>
        </tr>
        <tr>
            <td colspan="3">
                @if(is_null($pdf->firma_vacunador))
                <p style="padding-top: 2.5em"><b>Firma: ________________________</b></p>
                @else
                <img src="{{asset('vacunadores/'.$pdf->firma_vacunador)}}" alt="">
                <p><b>Firma: ________________________</b></p>
                @endif
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td colspan="3"><b>INSTITUCIÓN PRESTADORA DE SERVICIOS SALUD (IPS):</b> Hospital Universitario de Santander </td>
        </tr>
        <tr>
            <td colspan="3"><b>DEPARTAMENTO/DISTRITO:</b> Santander <b> MUNICIPIO:</b> Bucaramanga</td>
        </tr>
    </table>
    <p class="text-justify"><b>Nota:</b> Cuando se trate de menores de edad, deberá firmar el menor junto con la persona que tiene la patria potestad, la representación legal o la custodia. La persona
        que no pueda o no sepa firmar podrá acudir a la firma a ruego, en los términos de ley. </p>
</body>
</html>
