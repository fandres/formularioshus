@extends('layouts.admin')
@section('content')
<!-- Info boxes -->
<div class="row">
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Registrados</span>
                <span class="info-box-number">{{$total}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-search"></i></span>

            <div class="info-box-content">
                <form action="{{url('buscarporfecha')}}">
                    @csrf
                <div class="col-mb-3">
                    <input type="date" name="buscarfecha" >
                </div>
                <div class="col-mb-3" style="padding-top: 0.5em">
                    <input type="submit" class="btn btn-success btn-xs" value="Buscar">
                </div>

            </form>

            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><b>Pacientes del {{$fecha}} <span class="badge badge-info right">Cant. {{$formularios->count()}}</span></b></h3>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped" data-page-length='25'>
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Paciente</th>
                            <th>Fecha Registro</th>
                            <th>¿Aceptó?</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($formularios as $formulario)
                        <tr>
                            <td>{{$contador = $contador+1}}</td>
                            <td>{{$formulario->nombres.' '.$formulario->apellidos}}</td>
                            <td>{{$formulario->fecha_consentimiento}}</td>
                            <td>{{$formulario->acepta}}</td>
                            <td><a href="{{url('pdf/'.$formulario->id)}}" target="black"><i class="fas fa-file-pdf "></i> Ver Pdf</a></td>
                        </tr>
                        @endforeach
                    <tfoot>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Paciente</th>
                            <th>Fecha Registro</th>
                            <th>¿Aceptó?</th>
                            <th>Opciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection
