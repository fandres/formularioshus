<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="_token" content="{{csrf_token()}}" />
    <title>HUS Vacuna</title>
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('js/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/awesome-animation.css') }}">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome-animation@1.1.1/css/font-awesome-animation.css"> --}}
    <style>
        #toast {
            position: fixed;
            z-index: 1;
            right: 2.5%;
            top: 15%;
            border: 1px solid rgba(0, 0, 0, .1);
            border-radius: .25rem;
            box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .1);
            max-width: 310px;
            width: 310px;
            opacity: 1;
            padding: 0.5em;
            height: 80px;
        }

        .fondocolor {
            background: rgb(22, 170, 231);
            background: linear-gradient(162deg, rgba(22, 170, 231, 0) 3%, rgba(9, 175, 243, 1) 30%, rgba(45, 81, 192, 0.5494572829131652) 100%);
        }

        .txtblanco {
            color: whitesmoke
        }

        table,
        th,
        td {
            border: 1px solid #dee2e6;
            border-collapse: collapse;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 720px !important;
            }
        }

        .rojo {
            color: red;
            font-size: 1.2em;
        }

        .asterisco {
            position: absolute;
            font-size: 3em;
            margin: -8px -5px 5px 5px;
        }

        .img {
            width: 30%;
            text-align: center;
            margin: -5% 50px 30px auto;
        }

        .centrar {
            justify-content: center !important;
        }

    </style>
</head>

<body>
    <div class="container">
        <div class="jumbotron fondocolor txtblanco">
            <img src="{{asset('banner.png')}}" class="img" alt="">
            <H3 class="display-6 text-center">CONSENTIMIENTO INFORMADO PARA LA APLICACIÓN DE LA VACUNA CONTRA EL SARS-CoV-2/COVID-19</H3>
        </div>
        <cite style="font-size: 12px">Campos que son obligatorios <span class="rojo faa-flash animated faa-slow asterisco">*</span></cite>
        {{-- <div class=""> --}}
        <div class="col-md-12">
            <div class="well well-sm">
                <div class="row centrar">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                            <input id="ciudad" type="text" placeholder="Ciudad *" class="form-control" value="Bucaramanga">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                            <cite>Fecha Consentimiento</cite>
                            <input id="fecha_consentimiento" type="date" placeholder="Fecha *" class="form-control" value="{{date("Y-m-d")}}">
                        </div>
                    </div>
                </div>
                <fieldset>
                    <legend class="text-center header">Datos de identificación de la persona</legend>
                    <div class="row centrar">
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-12">
                                <cite>Tipo Cédula <span class="rojo faa-flash animated faa-slow">*</span></cite>
                                <select name="tipocedula" id="tipocedula" class="form-control">
                                    <option value=""></option>
                                    <option value="CC">CC</option>
                                    <option value="CE">CE</option>
                                    <option value="TI">TI</option>
                                    <option value="RC">RC</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <div class="col-md-12">
                                <cite>Documento<span class="rojo faa-flash animated faa-slow">*</span></cite>
                                <input id="cedula" type="text" placeholder="Campo obligatorio *" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></span>
                                <cite>Nombres<span class="rojo faa-flash animated faa-slow">*</span></cite>
                                <input id="nombres" type="text" placeholder="Campo obligatorio *" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Apellidos<span class="rojo faa-flash animated faa-slow">*</span></cite>
                                <input id="apellidos" type="text" placeholder="Campo obligatorio *" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Eps Afiliado<span class="rojo faa-flash animated faa-slow">*</span></cite>
                                <input id="eps" type="text" placeholder="Campo obligatorio *" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Fecha Nacimiento<span class="rojo faa-flash animated faa-slow">*</span></cite>
                                <input id="fecha_nacimiento" type="date" placeholder="Fecha nacimiento *" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Edad<span class="rojo faa-flash animated faa-slow">*</span></cite>
                                <input id="edad" type="number" placeholder="Campo obligatorio *" class="form-control">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <h3 class="text-center">Información Previa</h3>
                        <p class="text-justify">
                            @include('vacunacion._informacionPrevia')
                        </p>
                    </div>
                    <!--row-->
                    <div class="row">
                        <table class="table text-justify">
                            <tbody>
                                <tr>
                                    <th colspan="2">Datos importantes de la vacunacion (Por favor, leer con detenimiento. Puede formular preguntas)</th>
                                </tr>
                                <tr>
                                    <td>Vacuna y Dosis</td>
                                    <td>Nombre de la Vacunaa aplicar: 
                                        <select name="nombre_vacuna" id="nombre_vacuna" style="width: 100px">
                                            <option value=""></option>
                                            <option value="Sinovac">Sinovac</option>
                                            <option value="Pfizer">Pfizer</option>
                                        </select> <span class="rojo faa-flash animated faa-slow">*</span>
                                        <br> Esquema que tiene esta vacuna: Dos Dosis
                                        <input type="radio" id="dosis" name="dosis" value="2"><span class="rojo faa-flash animated faa-slow">*</span>
                                        o dosis única
                                        <input type="radio" id="dosis" name="dosis" value="1"><span class="rojo faa-flash animated faa-slow">*</span></td>
                                </tr>
                                <tr>
                                    <td>¿Comó se aplica</td>
                                    <td>Administración vía intramuscular en el brazo (tercio medio del musculo deltoides)</td>
                                </tr>
                                <tr>
                                    <td>Beneficios</td>
                                    <td>Prevención de la enfermedad COVID-19, causada por el virus SARS-CoV-2. Reducción de la severidad de la enfermedad en caso de presentarse.
                                        Potencial protección del entorno familiar v los allegados.
                                    </td>
                                </tr>
                                <tr>
                                    <td>Riesgos</td>
                                    <td>Presentación de efectos adversos a corto y mediano plazo posterior a su aplicación como: dolor en el sitio de inyección, dolor de cabeza (cefalea, articulaciones (artralgia) , muscular (mialgia); fatiga (cansancio ); resfriado; fiebre (pirexias;)enrojecimiento e inflamación leve en el lugar de la inyección; inflamación de los ganglios (linfadenopatía); malestar general; sensación de adormecimiento en las extremidades, reacciones alérgicas leves, moderadas o severas.
                                        Estos no se presentan en todas las personas.
                                    </td>
                                </tr>
                                <tr>
                                    <td>Alternativas</td>
                                    <td>A la fecha no se ha identificado otra medida farmacológica más eficaz que la vacunación para la prevención de la COVID-19.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--row-->
                    <div class="row">
                        <h3>Expresión de la voluntad</h3><br>
                        <br>
                        <p class="text-justify">
                            @include('vacunacion._expresionVolunta')
                        </p>
                        <p>En consecuencia, decido <b>ACEPTAR</b> <input type="radio" id="acepta" name="acepta" value="SI"><span class="rojo faa-flash animated faa-slow">*</span> Que se me aplique la vacuna
                            <b>NO ACEPTAR:</b> <input type="radio" id="acepta" name="acepta" value="NO"><span class="rojo faa-flash animated faa-slow">*</span> que se me aplique la vacuna.
                        </p>
                        <div class="col-sm-12" id="noacepto" style="justify-content: center;display:none">
                            <div class="form-group">
                                <textarea name="razon_nofirma" id="razon_nofirma" cols="30" rows="3" style="width: 100%" class="form-control" placeholder="Razón por la cual no firma *"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Correo Electrónico</cite>
                                <input id="email" type="email" placeholder="No es Obligatorio" class="form-control">
                            </div>
                        </div>
                    </div>
                    <center>
                        <h4><i class="fas fa-file-signature"></i><span class="rojo faa-flash animated faa-slow">*</span></h4>
                        <p>Firma:</p>
                        <div class="">
                            <div class="wrapper">
                                <canvas id="firma_empleado" class="firma_empleado" width=450 height=200 style="border-top: 1px solid #ced4da;
                          border-right: 1px solid #ced4da;
                          border-bottom: 1px solid #ced4da;
                          border-left: 1px solid #ced4da;"></canvas>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-secondary" id="clear">Limpiar</button>
                        </div>
                    </center>
                    <!--DATOS DEL ACUDIENTE-->
                    <div class="row">
                        <p>En caso de requerirse, identificación y firma de quien tiene la patria potestad, la representación legal o la custoda:</p>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></span>
                                <cite>Nombres Acudiente</cite>
                                <input id="nombres_acudiente" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></i></span>
                                <cite>Apellidos Acudiente</cite>
                                <input id="apellidos_acudiente" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></span>
                                <cite>Tipo Documento</cite>
                                <select name="tipocedula_acudiente" id="tipocedula_acudiente" class="form-control">
                                    <option value=""></option>
                                    <option value="CC">CC</option>
                                    <option value="CE">CE</option>
                                    <option value="TI">TI</option>
                                    <option value="RC">RC</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></span>
                                <cite>Documento Acudiente</cite>
                                <input id="cedula_acudiente" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"></span>
                                <cite>Lugar Expedición</cite>
                                <input id="lugar_expecedula_acudiente" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <center>
                        <h4><i class="fas fa-file-signature"></i></h4>
                        <p>Firma:</p>
                        <div class="">
                            <div class="wrapper">
                                <canvas id="firma_acudiente" class="firma_empleado" width=450 height=200 style="border-top: 1px solid #ced4da;
                          border-right: 1px solid #ced4da;
                          border-bottom: 1px solid #ced4da;
                          border-left: 1px solid #ced4da;"></canvas>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-secondary" id="clearAcudiente">Limpiar</button>
                        </div>
                    </center>
                    <hr>
                    @php
                    $vacunadores = DB::table('vacunadores')->get();
                    @endphp
                   <div class="row centrar">
                    
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"></span>
                            <cite>Seleccione el vacunador<span class="rojo faa-flash animated faa-slow">*</span></cite>
                            <select name="id_vacunador" id="id_vacunador" class="form-control">
                                <option value=""></option>
                                @foreach($vacunadores as $vacunador)
                                <option value="{{$vacunador->id}}">{{$vacunador->nombres_vacunador.' '.$vacunador->apellidos_vacunador}}</option>
                                @endforeach
                            </select>
                        </div>
                    
                   </div>
                    <hr>
                    <h3>Datos de la Institución</h3> <br>
                    <p><b>Institución prestadora de servicios salud (IPS):</b> Hospital Universitario de Santander</p>
                    <p><b>Departamento/Distrito:</b> Santander </p> <b> Municipio: </b> Bucaramanga</p>
                    <cite><b>Nota:</b> Cuanto se trate de menores de edad, deberá firmar el menor junto con la persona que tiene la patria potestad, la representación legal o la custodia. La persona que no pueda o no sepa firmar podrá acudir a la firma a ruego, en los términos de ley.</cite>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <footer style="padding-bottom: 1em;padding-top: 1em;">
                                <button class="btn btn-primary btn-lg btn-block fondocolor" id="save">Guardar</button>
                                <button class="btn btn-warning btn-lg btn-block" style="display: none" id="guardando">Guardando información ... <i class="fas fa-spinner fa-pulse"></i></button>
                            </footer>
                        </div>
                    </div>
                    {{-- <div class="alert alert-success" style="display:none"></div> --}}
                    <div class="alert alertTime alert-success alert-dimdissible" style="display: none;z-index:100" id="toast">
                        <i class="fas fa-check-circle"></i>
                    </div>
                    <div class="error alertTime alert-danger alert-dimdissible" style="display: none" id="toast">
                        <i class="fas fa-exclamation-triangle  faa-wrench animated faa-fast"></i> Existen Campos vacios
                    </div>

                </fieldset>
            </div>
        </div>
        {{-- </div>
        <!--1 row--> --}}
    </div>
    <!--
  <br>
  <div class="container">
    <div class="jumbotron">

      <h1 class="display-6">Signature pad library</h1>
      <p class="lead">Simple form made with Laravel framework and Ajax POST method</p>

      <div class="alert alert-success" style="display:none"></div>
        <div class="wrapper">
          <canvas id="firma_empleado" class="firma_empleado" width=400 height=200></canvas>
        </div>

        <br>
        <input type="text" id="nombres" required>
        <input type="text" id="apellidos" required>
        <button class="btn btn-primary" id="save">Guardar</button>
        <button class="btn btn-secondary" id="clear">Limpiar</button>
    </div>
  </div>
-->
    <!-- Link to js file in folder app and CDN used for the signature pad :  Jquery, ajax, signature pad -->
    <script src="{{ url('js/app.js') }} " charset="utf-8"></script>
    {{-- <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"> </script>--}}
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script> --}}
    <script src="{{asset('js/popper.min.js')}}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script> --}}
    <script src="{{asset('js/signature.js')}}"></script>
    <!-- AJAX to save signature pad content -->
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            var canvas = document.getElementById('firma_empleado');
            var canvasAcudiente = document.getElementById('firma_acudiente');
            //var canvasVacunador = document.getElementById('firma_vacunador');

            var signaturePad = new SignaturePad(canvas, {});
            var signaturePadAcudiente = new SignaturePad(canvasAcudiente, {});
           // var signaturePadVacunador = new SignaturePad(canvasVacunador, {});

            var saveButton = document.getElementById('save');
            var clearButton = document.getElementById('clear');
            var clearButtonAcudiente = document.getElementById('clearAcudiente');
            //var clearButtonVacunador = document.getElementById('clearVacunador');

            //var name = document.getElementById('txtNombre');
            //var name = $('#txtNombre').val();
            //var acude =  signaturePadAcudiente.toDataURL('image/png');

            saveButton.addEventListener('click', function(event) {
                // if (signaturePadAcudiente.isEmpty()) {
                //     var firmaAcudiente;
                // }
                // else{
                //     var firmaAcudiente = signaturePadAcudiente.toDataURL('image/png');
                // }
                var firmaAcudiente = signaturePadAcudiente.toDataURL('image/png');
                var firmapaciente = signaturePad.toDataURL('image/png');
                
               // var firmavacunador = signaturePadVacunador.toDataURL('image/png');
                //console.log(signaturePadAcudiente.toDataURL('image/png'));
                //if (signaturePad.isEmpty() || signaturePadVacunador.isEmpty()) {
                if (signaturePad.isEmpty()) {
                    alert("Firma Obligatoria");


                } else {
                    $('#save').hide();
                    $('#guardando').fadeIn();
                    var tipocedula = $('#tipocedula').val();
                    var cedula = $('#cedula').val();
                    var nombres = $('#nombres').val();
                    var apellidos = $('#apellidos').val();
                    var fecha_nacimiento = $('#fecha_nacimiento').val();
                    var edad = $('#edad').val();
                    var fecha_consentimiento = $('#fecha_consentimiento').val();
                    var ciudad = $('#ciudad').val();
                    var eps = $('#eps').val();
                    var nombre_vacuna = $('#nombre_vacuna').val();
                    var dosis = $('#dosis:checked').val();
                    var acepta = $('#acepta:checked').val();
                    var razon_nofirma = $('#razon_nofirma').val();
                    //ACUDIENTE
                    var nombres_acudiente = $('#nombres_acudiente').val();
                    var apellidos_acudiente = $('#apellidos_acudiente').val();
                    var tipocedula_acudiente = $('#tipocedula_acudiente').val();
                    var cedula_acudiente = $('#cedula_acudiente').val();
                    var lugar_expecedula_acudiente = $('#lugar_expecedula_acudiente').val();
                    //VACUNADOR 
                    var id_vacunador = $('#id_vacunador').val();
                    // var nombres_vacunador = $('#nombres_vacunador').val();
                    // var apellidos_vacunador = $('#apellidos_vacunador').val();
                    // var tipocedula_vacunador = $('#tipocedula_vacunador').val();
                    // var cedula_vacunador = $('#cedula_vacunador').val();
                    // var lugar_expecedula_vacunador = $('#lugar_expecedula_vacunador').val();

                    var email = $('#email').val();
                    $.ajax({
                        url: "{{ url('/signaturepadstore') }}"
                        , method: 'post'
                        , data: {
                            signature: firmapaciente
                            , signaturePadAcudiente: firmaAcudiente
                            , nombres: nombres
                            , apellidos: apellidos
                            , email: email
                            , tipocedula: tipocedula
                            , cedula: cedula
                            , fecha_nacimiento: fecha_nacimiento
                            , edad : edad
                            , fecha_consentimiento: fecha_consentimiento
                            , ciudad: ciudad
                            , eps: eps
                            , nombre_vacuna: nombre_vacuna
                            , dosis: dosis
                            , acepta: acepta
                            , razon_nofirma: razon_nofirma
                            , nombres_acudiente: nombres_acudiente
                            , apellidos_acudiente: apellidos_acudiente
                            , tipocedula_acudiente: tipocedula_acudiente
                            , cedula_acudiente: cedula_acudiente
                            , lugar_expecedula_acudiente: lugar_expecedula_acudiente
                            , id_vacunador : id_vacunador
                            // , nombres_vacunador: nombres_vacunador
                            // , apellidos_vacunador: apellidos_vacunador
                            // , tipocedula_vacunador: tipocedula_vacunador
                            // , cedula_vacunador: cedula_vacunador
                            // , lugar_expecedula_vacunador: lugar_expecedula_vacunador
                        , }
                        , success: function(result) {
                            jQuery('.alert').show();
                            jQuery('.alert').html(result.success);
                            $('#nombres').val('');
                            $('#apellidos').val('');
                            $('#email').val('');
                            $('#fecha_nacimiento').val('');
                            $('#edad').val('');
                            $('#cedula').val('');
                            $('#tipocedula').val('');
                            $('#eps').val('');
                            $('#nombre_vacuna').val('');
                            //$('#dosis').attr('checked', false);
                            $('input[type=radio]').prop('checked', false);
                            $('#razon_nofirma').val('');
                            $('#nombres_acudiente').val('');
                            $('#apellidos_acudiente').val('');
                            $('#cedula_acudiente').val('');
                            $('#tipocedula_acudiente').val('');
                            $('#lugar_expecedula_acudiente').val('');
                            $('#id_vacunador').val('');
                            // $('#nombres_vacunador').val('');
                            // $('#apellidos_vacunador').val('');
                            // $('#cedula_vacunador').val('');
                            // $('#tipocedula_vacunador').val('');
                            // $('#lugar_expecedula_vacunador').val('');
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                            signaturePad.clear();
                            signaturePadAcudiente.clear();
                           // signaturePadVacunador.clear();
                            setTimeout(function() {
                                $(".alertTime").fadeOut(1500);
                            }, 2000);
                            $('html, body').animate({scrollTop:0}, 'slow');
                        }
                        , error: function(msj) {
                            console.log(msj.responseJSON);
                            // $('.error').html(msj.success);
                            $('.error').show();
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                            setTimeout(function() {
                                $(".alertTime").fadeOut(3500);
                            }, 3500);
                        }
                    }); //respuesta
                } // cierre else
            }); //evento boton

            clearButton.addEventListener('click', function() {
                signaturePad.clear();
            });
            clearButtonAcudiente.addEventListener('click', function() {
                signaturePadAcudiente.clear();
            });
            // clearButtonVacunador.addEventListener('click', function() {
            //     signaturePadVacunador.clear();

            // });

        });

    </script>
    <script>
        $('input:radio[name="acepta"]').change(
            function() {
                if (this.checked && this.value == 'NO') {
                    $('#noacepto').fadeIn();
                }
                if (this.checked && this.value == 'SI') {
                    $('#noacepto').fadeOut();
                    $('#razon_nofirma').val('');
                }
            });

    </script>
</body>

</html>
