@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="{{url('vacunador')}}" class="btn btn-success"><i class="fas fa-user-plus"></i> Registrar</a>
              </div>
            
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Vacunadores</th>
                            <th>Fecha Registro</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($vacunadores as $vacunador)
                        <tr>
                            <td>{{$contador = $contador+1}}</td>
                            <td>{{$vacunador->nombres_vacunador.' '.$vacunador->apellidos_vacunador}}</td>
                            <td>{{ \Carbon\Carbon::parse($vacunador->created_at)->format('Y-m-d')}}</td>
                            <td>--</td>
                            {{-- <td><a href="{{url('pdf/'.$formulario->id)}}" target="black"><i class="fas fa-file-pdf "></i></a></td> --}}
                        </tr>
                        @endforeach
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection
