@extends('layouts.admincapacitaciones')
@section('contenido')
<div id="app">
    <crearactividad-component 
    v-bind:mediosolicitud="{{$mediosolicitud}}"
    v-bind:lugares="{{$lugares}}"
    v-bind:tipoactividad="{{$tipoactividad}}"
    v-bind:servicios="{{$servicios}}"
    >
</crearactividad-component>
</div>
@endsection