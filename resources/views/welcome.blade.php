<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    {{--
    <meta name="viewport" content="width=device-width, initial-scale=1"> --}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Talento Humano') }}</title>



    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
    <style>
        .indexwrapper {
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            width: 100%;
            min-height: 100%;
            padding: 20px;
            background: rgba(4, 40, 68, 0.85);
        }
        .cajas
        {
            border-radius: 0.5em;
            padding: 0.5em;
            width: 80%;
            height: 150px;
        }
    </style>
</head>

<body class="hold-transition login-page" style="background: url({{ asset('img/hus2.png')}}) 50% fixed;
background-size: cover;">
    <div class="wrapper indexwrapper">
        <div class="login-box" style="width: 100% !important">
            <div class="row">
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <div class="card mb-2 bg-gradient-dark cajas">
                        <h4>Formulario Carnets, Cintas</h4>
                        <cite>Registro de entregas y devoluciones de carnets y cintas por parte de docentes y estudiantes</cite>
                        <div class="row">
                        <div class=" col-xs-6 p-2">
                            <a href="{{url('viewformcarnetentrega')}}" class="btn btn-success">Entregar</a>
                        </div>
                        <div class=" col-xs-6 p-2">
                            <a href="{{url('ajaxdevolucion')}}" class="btn btn-success">Devolver</a>
                        </div>
                    </div>
                        
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-4">
                    <div class="card mb-2 bg-gradient-dark cajas">
                        <h4>Formulario Vacunación</h4>
                        <cite>Registro de pacientes y empleados Vacunados Covid-19</cite>
                        <button>Abrir</button>
                    </div>
                </div>
            </div><!-- /.row -->
            
        </div>
    </div>
    <!-- /.login-box -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
</body>

</html>