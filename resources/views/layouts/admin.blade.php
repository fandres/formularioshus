<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{csrf_token()}}" />
    <title>Formulario</title>

    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
    
    <link rel="stylesheet" href="{{ asset('css/awesome-animation.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- Toastr -->
    {{-- <livewire:styles>
    <livewire:scripts> --}}
    <style>
       #toast {
            position: fixed;
            z-index: 1;
            right: 2.5%;
            top: 15%;
            border: 1px solid rgba(0, 0, 0, .1);
            border-radius: .25rem;
            box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .1);
            max-width: 310px;
            width: 310px;
            opacity: 1;
            padding: 0.5em;
            height: 80px;
        }

        .fondocolor {
            background: rgb(22, 170, 231);
            background: linear-gradient(162deg, rgba(22, 170, 231, 0) 3%, rgba(9, 175, 243, 1) 30%, rgba(45, 81, 192, 0.5494572829131652) 100%);
        }

        .txtblanco {
            color: whitesmoke
        }

        table,
        th,
        td {
            border: 1px solid #dee2e6;
            border-collapse: collapse;
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 720px !important;
            }
        }

        .rojo {
            color: red;
            font-size: 1.2em;
        }

        .asterisco {
            position: absolute;
            font-size: 3em;
            margin: -8px -5px 5px 5px;
        }

        .img {
            width: 30%;
            text-align: center;
            margin: -5% 50px 30px auto;
        }

        .centrar {
            justify-content: center !important;
        }
        .logoutpower {
            border-left-style: groove;
            text-decoration: none !important;
            padding-left: 0.5em;
            color: black;
        }

        .Fonttabla {
            font-size: 12px;
        }

    </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                {{-- <li class="nav-item d-none d-md-inline-block">
                    <a href="../../index3.html" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-md-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li> --}}
            </ul>
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-comments"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="{{asset('dist/img/user1-128x128.jpg')}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Brad Diesel
                                        <span class="float-right text-md text-danger"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-md">Call me whenever you can...</p>
                                    <p class="text-md text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="{{asset('dist/img/user8-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        John Pierce
                                        <span class="float-right text-md text-muted"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-md">I got your message bro</p>
                                    <p class="text-md text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <div class="media">
                                <img src="{{asset('dist/img/user3-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Nora Silvester
                                        <span class="float-right text-md text-warning"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-md">The subject goes here</p>
                                    <p class="text-md text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                    </div>
                </li>
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-md">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-md">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-md">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li> --}}
                <li>
                    <a href="{{ route('logout') }}" class="text-center logoutpower" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" class="dropdown-item">
                        <i class="fas fa-power-off fa-2x" style="color:#b91212"></i> Cerrar Sesión </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="../../index3.html" class="brand-link">
                <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">HUS</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">User</a>
                    </div>
                </div>
                 <!-- Sidebar Menu -->
                 <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                   <li class="nav-item">
                    <a href="{{url('admin')}}" class="nav-link {{ Request::is('admin') ? 'activolink' : '' }}">
                        <i class="fas fa-clipboard-list"></i>
                        <p>
                            Listado Pacientes
                        </p>
                    </a>
                </li>
                        <li class="nav-item">
                            <a href="{{url('indexvacunador')}}" class="nav-link {{ Request::is('vacunador') ? 'activolink' : '' }}">
                                <i class="fas fa-stethoscope"></i>
                                <p>
                                    Vacunadores
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <h1>
                                @yield('title')
                            </h1>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="">
                    @if(session()->has('mensajeOk'))
                    <div class="alert alert-success alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-check"></i> Ok! </h4>
                        {{session('mensajeOk')}}
                    </div>
                    @elseif((session()->has('delete')))
                    <div class="alert alert-info alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-info"></i> !!Proceso Realizado</h4>
                        {{session('delete')}}
                    </div>
                    @elseif((session()->has('error')))
                    <div class="alert alert-danger alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-ban"></i> ALERTA! </h4>
                        {{session('error')}}
                    </div>
                    @endif
                    @yield('content')
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark" style="padding:2em">

        </aside>
        <!-- /.control-sidebar -->
        <footer class="main-footer">
            <div class="float-right d-none d-md-block">
                <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2021 <a href="http://www.hus.gov.co">Hospital Universitario de Santander</a>.</strong>
        </footer>
    </div>
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> --}}
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    {{-- <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script> --}}
    <script src="{{ asset('js/dropdown.js') }}"></script>
    {{-- <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script> --}}
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- Link to js file in folder app and CDN used for the signature pad :  Jquery, ajax, signature pad -->
    {{-- <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"> </script>--}}
  

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script> --}}
    <script src="{{asset('js/popper.min.js')}}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script> --}}
    <script src="{{asset('js/signature.js')}}"></script>
    <!-- AJAX to save signature pad content -->
  
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").fadeOut(1500);
            }, 2000);

            /*setTimeout(function() {
                $(".content2").fadeIn(1500);
            },6000);*/
        });

    </script>
    
    {{-- <script>
        $('[data-mask]').inputmask()
        var first_select = document.getElementById('estado').value;
        if (first_select == 2) {
            $("#retirado").show("fast");
        }
        $(function() {
            $("#estado").change(function() {
                if ($(this).val() === "2") {
                    $("#retirado").show("fast");
                    $("#fecharetiro").prop('required', true);
                } else {
                    $("#retirado").hide("fast");
                    $("#fecharetiro").val('');
                    $("#fecharetiro").prop('required', false);
                }
            });
        });

    </script> --}}
    <script>
        $(function() {
            $('#example1').DataTable({
                "language": {
                    "sProcessing": "Procesando..."
                    , "sLengthMenu": "Mostrar _MENU_ registros"
                    , "sZeroRecords": "No se encontraron resultados"
                    , "sEmptyTable": "Ningún dato disponible en esta tabla"
                    , "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
                    , "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros"
                    , "sInfoFiltered": "(filtrado de un total de _MAX_ registros)"
                    , "sInfoPostFix": ""
                    , "sSearch": "Buscar:"
                    , "sUrl": ""
                    , "sInfoThousands": ","
                    , "sLoadingRecords": "Cargando..."
                    , "oPaginate": {
                        "sFirst": "Primero"
                        , "sLast": "Último"
                        , "sNext": "Siguiente"
                        , "sPrevious": "Anterior"
                    }
                    , "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente"
                        , "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    
    </script>
    
    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

           // var canvas = document.getElementById('firma_empleado');
           // var canvasAcudiente = document.getElementById('firma_acudiente');
            var canvasVacunador = document.getElementById('firma_vacunador');

            //var signaturePad = new SignaturePad(canvas, {});
            //var signaturePadAcudiente = new SignaturePad(canvasAcudiente, {});
            var signaturePadVacunador = new SignaturePad(canvasVacunador, {});

            var saveButton = document.getElementById('save');
           // var clearButton = document.getElementById('clear');
            //var clearButtonAcudiente = document.getElementById('clearAcudiente');
            var clearButtonVacunador = document.getElementById('clearVacunador');

            //var name = document.getElementById('txtNombre');
            //var name = $('#txtNombre').val();
            //var acude =  signaturePadAcudiente.toDataURL('image/png');

            saveButton.addEventListener('click', function(event) {
               // var firmapaciente = signaturePad.toDataURL('image/png');
               // var firmaAcudiente = signaturePadAcudiente.toDataURL('image/png');
                var firmavacunador = signaturePadVacunador.toDataURL('image/png');
                //console.log(signaturePadAcudiente.toDataURL('image/png'));
                //if (signaturePad.isEmpty() || signaturePadVacunador.isEmpty()) {
                if (signaturePadVacunador.isEmpty()) {
                    alert("Firma Obligatoria");


                } else {
                    $('#save').hide();
                    $('#guardando').fadeIn();
                    // var tipocedula = $('#tipocedula').val();
                    // var cedula = $('#cedula').val();
                    // var nombres = $('#nombres').val();
                    // var apellidos = $('#apellidos').val();
                    // var fecha_nacimiento = $('#fecha_nacimiento').val();
                    // var edad = $('#edad').val();
                    // var fecha_consentimiento = $('#fecha_consentimiento').val();
                    // var ciudad = $('#ciudad').val();
                    // var eps = $('#eps').val();
                    // var nombre_vacuna = $('#nombre_vacuna').val();
                    // var dosis = $('#dosis:checked').val();
                    // var acepta = $('#acepta:checked').val();
                    // var razon_nofirma = $('#razon_nofirma').val();
                    // //ACUDIENTE
                    // var nombres_acudiente = $('#nombres_acudiente').val();
                    // var apellidos_acudiente = $('#apellidos_acudiente').val();
                    // var tipocedula_acudiente = $('#tipocedula_acudiente').val();
                    // var cedula_acudiente = $('#cedula_acudiente').val();
                    // var lugar_expecedula_acudiente = $('#lugar_expecedula_acudiente').val();
                    //VACUNADOR
                    var nombres_vacunador = $('#nombres_vacunador').val();
                    var apellidos_vacunador = $('#apellidos_vacunador').val();
                    var tipocedula_vacunador = $('#tipocedula_vacunador').val();
                    var cedula_vacunador = $('#cedula_vacunador').val();
                    var lugar_expecedula_vacunador = $('#lugar_expecedula_vacunador').val();

                    //var email = $('#email').val();
                    $.ajax({
                        url: "{{ url('/signaturepadstoreVacunador') }}"
                        , method: 'post'
                        , data: {
                              signaturePadVacunador: firmavacunador
                            , nombres_vacunador: nombres_vacunador
                            , apellidos_vacunador: apellidos_vacunador
                            , tipocedula_vacunador: tipocedula_vacunador
                            , cedula_vacunador: cedula_vacunador
                            , lugar_expecedula_vacunador: lugar_expecedula_vacunador
                        , }
                        , success: function(result) {
                            jQuery('.alert').show();
                            jQuery('.alert').html(result.success);
                            
                            $('#nombres_vacunador').val('');
                            $('#apellidos_vacunador').val('');
                            $('#cedula_vacunador').val('');
                            $('#tipocedula_vacunador').val('');
                            $('#lugar_expecedula_vacunador').val('');
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                           // signaturePad.clear();
                            //signaturePadAcudiente.clear();
                            signaturePadVacunador.clear();
                            setTimeout(function() {
                                $(".alertTime").fadeOut(1500);
                            }, 2000);
                            $('html, body').animate({scrollTop:0}, 'slow');
                        }
                        , error: function(msj) {
                            console.log(msj.responseJSON);
                            // $('.error').html(msj.success);
                            $('.error').show();
                            $('#guardando').fadeOut();
                            $('#save').fadeIn();
                            setTimeout(function() {
                                $(".alertTime").fadeOut(3500);
                            }, 3500);
                        }
                    }); //respuesta
                } // cierre else
            }); //evento boton

            clearButtonVacunador.addEventListener('click', function() {
                signaturePadVacunador.clear();

            });

        });

    </script>
</body>
</html>
