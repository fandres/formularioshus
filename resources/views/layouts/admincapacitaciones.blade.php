<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- <meta name="_token" content="{{csrf_token()}}" /> --}}
    <title>Formulario</title>
    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilos.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">    
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <a href="{{ route('logout') }}" class="text-center logoutpower" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" class="dropdown-item">
                        <i class="fas fa-power-off fa-2x" style="color:#b91212"></i> Cerrar Sesión </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ route('login') }}" class="brand-link">
                <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">HUS</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{asset('user.png')}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{ Auth::user()->name}}</a>
                    </div>
                </div>
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                   @if(auth()->user()->hasRole(['ingesoporte']))
                        <li class="nav-item">
                            <a href="{{url('viewactividades')}}"
                                class="nav-link {{ Request::is('viewactividades') ? 'activolink' : '' }}">
                                <i class="far fa-list-alt"></i>
                                <p>
                                    Actividades
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('viewregistrohc')}}"
                                class="nav-link {{ Request::is('viewregistrohc') ? 'activolink' : '' }}">
                                <i class="far fa-list-alt"></i>
                                <p>
                                    HC CR/AC
                                </p>
                            </a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a href="{{url('viewcrearcapacitaciones')}}"
                                class="nav-link {{ Request::is('viewcrearcapacitaciones') ? 'activolink' : '' }}">
                                <i class="fas fa-folder-plus"></i>
                                <p>
                                    Capacitación/Reunión
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('viewreportescapacitacion')}}"
                                class="nav-link {{ Request::is('viewreportescapacitacion') ? 'activolink' : '' }}">
                                <i class="fas fa-info-circle"></i>
                                <p>
                                    Reportes
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link {{ Request::is('/') ? 'activolink' : '' }}"
                                target="black">
                                <i class="far fa-list-alt"></i>
                                <p>
                                    Formulario C/R
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <h1>
                                @yield('title')
                            </h1>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="">
                    @if(session()->has('mensajeOk'))
                    <div class="alert alert-success alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-check"></i> Ok! </h4>
                        {{session('mensajeOk')}}
                    </div>
                    @elseif((session()->has('notificacion')))
                    <div class="alert alert-info alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-info"></i> !!</h4>
                        {{session('notificacion')}}
                    </div>
                    @elseif((session()->has('error')))
                    <div class="alert alert-danger alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-ban"></i> ALERTA! </h4>
                        {{session('error')}}
                    </div>
                    @endif
                    @yield('contenido')
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark" style="padding:2em">

        </aside>
        <!-- /.control-sidebar -->
        <footer class="main-footer">
            <div class="float-right d-none d-md-block">
                <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2021 <a href="http://www.hus.gov.co">Hospital Universitario de
                    Santander</a>.</strong>
        </footer>
    </div>
    <script src="{{url("/js/app.js")}}"></script>
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{asset('js/signature.js')}}"></script>
    <script type="text/javascript" defer>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").fadeOut(1500);
            }, 2000);
        });
    </script>
    <script defer>
        function alertasesion() {
            Swal.fire({
                title: 'Sesión cerrada'
                , text: "Por favor actualice la página"
                , icon: 'warning'
                , confirmButtonColor: '#3085d6'
                , confirmButtonText: 'Refrescar'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "/login";
                }
            })
        }
        function alertaerror() {
            Swal.fire({
                title: 'Se produjo un error'
                , text: "Por favor actualice la página"
                , icon: 'error'
                , confirmButtonColor: '#3085d6'
                , confirmButtonText: 'Refrescar'
            }).then((result) => {
                if (result.isConfirmed) {
                    location.reload();
                }
            })
        }
    </script>
</body>
</html>