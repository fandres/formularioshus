<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{csrf_token()}}" />
    <title>Formulario</title>
    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
{{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('css/awesome-animation.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
            <style>
                #toast {
                    position: fixed;
                    z-index: 1;
                    right: 2.5%;
                    top: 15%;
                    border: 1px solid rgba(0, 0, 0, .1);
                    border-radius: .25rem;
                    box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .1);
                    max-width: 310px;
                    width: 310px;
                    opacity: 1;
                    padding: 0.5em;
                    height: 80px;
                }

                .fondocolor {
                    background: rgb(22, 170, 231);
                    background: linear-gradient(162deg, rgba(22, 170, 231, 0) 3%, rgba(9, 175, 243, 1) 30%, rgba(45, 81, 192, 0.5494572829131652) 100%);
                }

                .txtblanco {
                    color: whitesmoke
                }

                table,
                th,
                td {
                    border: 1px solid #dee2e6;
                    border-collapse: collapse;
                }

                @media (min-width: 1200px) {
                    .container {
                        max-width: 720px !important;
                    }
                }

                .rojo {
                    color: red;
                    font-size: 1.2em;
                }

                .asterisco {
                    position: absolute;
                    font-size: 3em;
                    margin: -8px -5px 5px 5px;
                }

                .img {
                    width: 30%;
                    text-align: center;
                    margin: -5% 50px 30px auto;
                }

                .centrar {
                    justify-content: center !important;
                }

                .logoutpower {
                    border-left-style: groove;
                    text-decoration: none !important;
                    padding-left: 0.5em;
                    color: black;
                }

                .Fonttabla {
                    font-size: 12px;
                }
            </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
            </ul>
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <a href="{{ route('logout') }}" class="text-center logoutpower" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" class="dropdown-item">
                        <i class="fas fa-power-off fa-2x" style="color:#b91212"></i> Cerrar Sesión </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ route('login') }}" class="brand-link">
                <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">HUS</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{asset('user.png')}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">{{ Auth::user()->name}}</a>
                    </div>
                </div>
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="{{url('/')}}" class="nav-link {{ Request::is('/') ? 'activolink' : '' }}">
                                <i class="fas fa-info-circle"></i>
                                <p>
                                    Agregar
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            {{-- <a href="{{url('listadesconformaciones')}}"
                                class="nav-link {{ Request::is('listadesconformaciones') ? 'activolink' : '' }}">
                                <i class="fas fa-clipboard-list"></i>
                                <p>
                                    Listado Desconfirmaciones
                                </p>
                            </a> --}}
                        </li>
                        <li class="nav-item">
                            {{-- <a href="{{route('desbloquear.index')}}"
                                class="nav-link {{ Request::is('desbloquear.index') ? 'activolink' : '' }}">
                                <i class="fas fa-clipboard-list"></i>
                                <p>
                                    Desbloquear Usuario DGH
                                </p>
                            </a> --}}
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <h1>
                                @yield('title')
                            </h1>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="">
                    @if(session()->has('mensajeOk'))
                    <div class="alert alert-success alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-check"></i> Ok! </h4>
                        {{session('mensajeOk')}}
                    </div>
                    @elseif((session()->has('notificacion')))
                    <div class="alert alert-info alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-info"></i> !!</h4>
                        {{session('notificacion')}}
                    </div>
                    @elseif((session()->has('error')))
                    <div class="alert alert-danger alert-dimdissible" id="toast">
                        <h4><i class="icon fas fa-ban"></i> ALERTA! </h4>
                        {{session('error')}}
                    </div>
                    @endif
                    @yield('contenido')
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark" style="padding:2em">

        </aside>
        <!-- /.control-sidebar -->
        <footer class="main-footer">
            <div class="float-right d-none d-md-block">
                <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2021 <a href="http://www.hus.gov.co">Hospital Universitario de
                    Santander</a>.</strong>
        </footer>
    </div>
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> --}}
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    {{-- <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script> --}}
    <script src="{{ asset('js/dropdown.js') }}"></script>
    {{-- <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script> --}}
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- Link to js file in folder app and CDN used for the signature pad :  Jquery, ajax, signature pad -->
    {{-- <script src="http://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"> </script>--}}


    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script> --}}
    <script src="{{asset('js/popper.min.js')}}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script> --}}
    <script src="{{asset('js/signature.js')}}"></script>
    <!-- AJAX to save signature pad content -->

    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").fadeOut(1500);
            }, 2000);

            /*setTimeout(function() {
                $(".content2").fadeIn(1500);
            },6000);*/
        });

    </script>
</body>

</html>