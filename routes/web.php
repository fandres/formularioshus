<?php

use Illuminate\Support\Facades\Route;
  
use App\Http\Controllers\SignaturePadController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\FormEntregaCarnetController;
use App\Http\Controllers\FormAdminEntregaCarnetController;
use App\Http\Controllers\Capacitaciones\FormCapacitacionesController;
use App\Http\Controllers\Capacitaciones\AdminCapacitacionController;
use App\Http\Controllers\ActividadesInge\ActividadesController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ActividadesInge\Actividades_registrohcController;
  
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/',[FormCapacitacionesController::class,'viewregistrarmecapacitacion']);
Route::get('adminredirect',[HomeController::class,'adminredirect']);

/* 
    RUTAS DEL FORMULARIO ENTREGA DE CARNET INSTITUCIONAL
*/

 Route::get('viewformcarnetentrega',[FormEntregaCarnetController::class,'viewEntrega']);
 Route::get('viewformcarnetdevolucion',[FormEntregaCarnetController::class,'viewDevolucion']);
 Route::get('viewformcarnetreimpresion',[FormEntregaCarnetController::class,'viewReimpresion']);
 Route::post('formentregacarnet',[FormEntregaCarnetController::class,'entrega']);
 Route::get('formdevolucioncarnet',[FormEntregaCarnetController::class,'devolucion']);
 Route::post('/consultadevolver',[FormEntregaCarnetController::class,'consultadevolver']);
 Route::get('/viewformcarnetadmin',[FormAdminEntregaCarnetController::class,'viewAdmin']);
 Route::post('devolviendoelementos',[FormEntregaCarnetController::class,'devolviendoelementos']);
 Route::post('reimprimiendoelementos',[FormEntregaCarnetController::class,'reimprimiendoelementos']);
 Route::post('excelfecha',[FormAdminEntregaCarnetController::class,'excelformfecha']);
 Route::post('excelinstitucion',[FormAdminEntregaCarnetController::class,'excelforminstitucion']);
 Route::post('excelprograma',[FormAdminEntregaCarnetController::class,'excelformprograma']);
 Route::get('fichafirmascarnet/{id}',[FormAdminEntregaCarnetController::class,'fichacarnets']);
/*--------------------------------------------------------------------------------------------- */

/* 
    RUTAS DEL FORMULARIO CAPACITACIONES
*/
// Route::get('viewregistrarmecapacitacion',[FormCapacitacionesController::class,'viewregistrarmecapacitacion']);
Route::post('viewformcapacitacion',[FormCapacitacionesController::class,'viewcapacitacion']);
Route::get('link/{codigo}',[FormCapacitacionesController::class,'viewcapacitacionurl']);
Route::post('capacitacion',[FormCapacitacionesController::class,'guardarcapacitacion']);
Route::get('viewcrearcapacitaciones',[AdminCapacitacionController::class,'viewcrearcapacitacion']);
Route::post('crearcapacitacion',[AdminCapacitacionController::class,'crearcapacitacion']);
Route::get('consultacapacitaciones',[AdminCapacitacionController::class,'consultacapacitaciones']);
Route::get('servicios',[AdminCapacitacionController::class,'servicios']);
Route::get('editarcapacitacion/{id}',[AdminCapacitacionController::class,'editarcapacitacion']);
Route::post('updatecapacitacion/{id}',[AdminCapacitacionController::class,'updatecapacitacion']);
Route::get('eliminarcapacitacion/{id}',[AdminCapacitacionController::class,'eliminarcapacitacion']);
Route::get('viewreportescapacitacion',[AdminCapacitacionController::class,'viewreportescapacitacion']);
Route::post('reportescapacitacion',[AdminCapacitacionController::class,'reportescapacitacion']);
Route::get('viewmodaldetallecabecera/{id}',[AdminCapacitacionController::class,'viewmodaldetallecabecera']);
Route::get('pruebascon',[AdminCapacitacionController::class,'pruebas']);
/*------------------ ACTIVIDADES DE LOS INGENIEROS---------------------*/
Route::get('viewactividades',[ActividadesController::class,'viewactividades']);
Route::post('createactividad',[ActividadesController::class,'createactividad']);
Route::get('consultactividades',[ActividadesController::class,'consultactividades']);
Route::get('editaractividad/{id}',[ActividadesController::class,'editaractividad']);
Route::post('updateactividad/{id}',[ActividadesController::class,'updateactividad']);
Route::get('eliminaractividad/{id}',[ActividadesController::class,'eliminaractividad']);
Route::get('validositienecargo',[ActividadesController::class,'validositienecargo']);
Route::post('asignarcargoinge',[ActividadesController::class,'asignarcargoinge']);
Route::get('obtenercargos',[ActividadesController::class,'obtenercargos']);
Route::post('updatecargoinge',[ActividadesController::class,'updatecargoinge']);
Route::get('viewregistrohc',[Actividades_registrohcController::class,'viewregistrohc']);
Route::get('getcargoshc',[Actividades_registrohcController::class,'getcargoshc']);
Route::get('getsubgerencias',[Actividades_registrohcController::class,'getsubgerencias']);
Route::get('getservicios',[Actividades_registrohcController::class,'getservicios']);
Route::get('registroshctable',[Actividades_registrohcController::class,'registroshctable']);
Route::post('createRegistroHc',[Actividades_registrohcController::class,'createRegistroHc']);
Route::get('editarregistrohc/{id}',[Actividades_registrohcController::class,'editarregistrohc']);
Route::post('updateregistrohc/{id}',[Actividades_registrohcController::class,'updateregistrohc']);
Route::get('eliminarregistrohc/{id}',[Actividades_registrohcController::class,'eliminarregistrohc']);
/* 
    RUTAS DEL FORMULARIO DE VACUNACIÓN
*/
Route::get('/formulario', function () {
    return view('vacunacion.formulario');
});
//Route::get('signaturepad', [SignaturePadController::class, 'index']);
//Route::post('signaturepad', [SignaturePadController::class, 'upload'])->name('signaturepad.upload');
Route::post('signaturepadstore', [SignaturePadController::class, 'guardar']);//->name('signaturepad.store');
Route::post('signaturepadstoreVacunador', [AdminController::class, 'guardar']);//->name('signaturepad.store');
Route::get('/pdf/{id}', [SignaturePadController::class, 'pdf']);//->name('signaturepad.store');
Route::get('admin', [AdminController::class, 'index']);//->name('signaturepad.store');
Route::get('vacunador', [AdminController::class, 'vacunador']);//->name('signaturepad.store');
Route::get('indexvacunador', [AdminController::class, 'indexvacunador']);//->name('signaturepad.store');
Route::get('buscarporfecha',[AdminController::class,'buscarporfecha']);
Route::get('/signature-pad', function () {
    return view('signature-pad');
});

Route::get('/pruebasignature', function () {
   return $hora = now()->format('H:m');
   $fecha = now()->format('Y-m-d');
});
//Route::post('/signature/post', 'SignatureController@store');
//Route::post('signature/post', [SignaturePadController::class, 'store']);
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/admin',function(){
//     return view('vacunacion.adminIndex');
// });
Route::get('/prueba', function (){
    
    $actividades = Actividades::findOrFail(10);
    return response()->json($actividades);
});
