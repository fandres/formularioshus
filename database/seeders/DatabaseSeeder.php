<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\models\User;
use DB;
class DatabaseSeeder extends Seeder

{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        //Admin
       
        DB::table('users')->insert([
            'name' => 'fabian',
            'email' =>'soportetecnologia@hus.gov.co',
            'password' => bcrypt('12345678'),
        ]);
        
       $capacitador = Role::create(['name' => 'capacitador']);
       $ingesoporte = Role::create(['name' => 'ingesoporte']);
       $carnets = Role::create(['name' => 'carnets']);
       $consultar_capacitaciones = Role::create(['name' => 'consultar_capacitaciones']);
       $admin = Role::create(['name' => 'admin']);

       $user = User::where('name','fabian')->first(); //Italo Morales
       $user->assignRole('capacitador');

    //    DB::table('capacitaciones_cabeceras')->insert([
    //     'id_user' => 1,
    //     'tema' => 'CCertihus',
    //     'fecha_capacitacion' => '2021-01-01',
    //     'horainicial' => '11:02:15',
    //     'horafinal' => '12:02:15',
    //     'tipo' => 'P',
    //     'servicios' => 'ufati'
    // ]);
    }
}
