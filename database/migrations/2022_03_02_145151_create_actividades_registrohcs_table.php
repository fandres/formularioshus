<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadesRegistrohcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades_registrohcs', function (Blueprint $table) {
            $table->id();
            $table->date('fechasolicitud');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->time('horasolicitud');
            $table->integer('id_subgerenciasolicitante');
            $table->integer('id_serviciosolicitante');
            $table->string('solicitante');
            $table->integer('id_cargosolicitante');
            //CR - AC - I
            $table->string('novedad');
            $table->string('codigohc');
            $table->string('nombrehc');
            $table->date('fechaentrega');
            $table->time('horaentrega');
            $table->text('criteriosnocumplidos');
            $table->text('criterioscumplidos');
            $table->char('evaluacion',1);
            // 1 = REALIZADO 2 = NO SE PUDO REALIZAR
            $table->char('estado',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades_registrohcs');
    }
}
