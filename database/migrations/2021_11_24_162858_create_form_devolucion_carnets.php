<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormDevolucionCarnets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_devolucion_carnets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_entrega_carnets');
            $table->foreign('form_entrega_carnets')->references('id')->on('form_entrega_carnets')->onDelete('cascade');
            // 0  No ha devuelto  1 Hizo la devolución
            $table->char('carnet',2)->default('3');
            $table->char('cinta',2)->default('3');
            $table->char('portacarnet',2)->default('3');
            $table->date('fecha_devolucion');
            $table->mediumText('firmaDevolucion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_devolucion_carnets');
    }
}
