<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormReimpresionCarnetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_reimpresion_carnets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_entrega_carnets');
            $table->foreign('form_entrega_carnets')->references('id')->on('form_entrega_carnets')->onDelete('cascade');
            $table->date('fecha_reimpresion');
            $table->char('carnet',2)->nullable();
            $table->char('cinta',2)->nullable();
            $table->char('portacarnet',2)->nullable();
            $table->string('observacion')->nullable();
            $table->mediumText('firmaReimpresion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_reimpresion_carnets');
    }
}
