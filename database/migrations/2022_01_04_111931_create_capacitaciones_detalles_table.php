<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionesDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitaciones_detalles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('capacitaciones_cabecera');
            $table->foreign('capacitaciones_cabecera')->references('id')->on('capacitaciones_cabeceras')->onDelete('cascade');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('documento');
            $table->char('tipodocumento',3);
            $table->string('cargo');
            $table->string('servicio_detalle');
            $table->string('empresa');
            $table->date('fecha_entrega');
            $table->mediumText('firmaEntrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacitaciones_detalles');
    }
}
