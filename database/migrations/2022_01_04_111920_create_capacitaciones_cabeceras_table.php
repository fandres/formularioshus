<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionesCabecerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitaciones_cabeceras', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->string('tema');
            $table->string('responsable');
            $table->string('servicio_cabecera');
            $table->string('codigo')->unique();
            $table->date('fecha_capacitacion');
            $table->time('horainicial');
            $table->time('horafinal');
            //TIPO => P = PRESENCION  V = VIRTUAL
            $table->char('tipo',1);
            //tiporc => R = REUNION  C = CAPACITACION
            $table->char('tiporc',1);
            $table->string('linkevaluacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacitaciones_cabeceras');
    }
}
