<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormEntregaCarnetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_entrega_carnets', function (Blueprint $table) {
            $table->id();
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('documento');
            $table->char('tipodocumento',3);
            $table->string('perfil');
            $table->string('programa');
            $table->string('institucion');
            //situaciones: 
            // carnet = NULL no se entrego 
            // carnet = 1  se entrego carnet
            // Carnet = 2  devolvió el carnet
            $table->char('carnet',1)->nullable();
            $table->char('cinta',1)->nullable();
            $table->char('portacarnet',1)->nullable();
            $table->char('esreimpresion',1)->nullable();
            $table->date('fecha_entrega');
            $table->mediumText('firmaEntrega');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_entrega_carnets');
    }
}
