<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormularioVacunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulario_vacunas', function (Blueprint $table) {
            $table->id();
            $table->char('tipocedula',2)->nullable();
            $table->string('cedula')->nullable();
            $table->string('nombres')->nullable();
            $table->string('apellidos')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->string('ciudad')->nullable();
            $table->date('fecha_consentimiento')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->integer('edad')->nullable();
            $table->string('eps')->nullable();
            $table->string('nombre_vacuna')->nullable();
            $table->string('dosis')->nullable();
            $table->char('acepta',2)->nullable();
            $table->text('razon_nofirma')->nullable();
            $table->string('firma_empleado')->nullable();
            /* datos acudiente*/
            $table->string('nombres_acudiente')->nullable();
            $table->string('apellidos_acudiente')->nullable();
            $table->char('tipocedula_acudiente',2)->nullable();
            $table->string('cedula_acudiente')->nullable();
            $table->string('lugar_expecedula_acudiente')->nullable();
            $table->string('firma_acudiente')->nullable();
            /*VACUNADOR*/ 
            $table->unsignedInteger('id_vacunador');
            $table->foreign('id_vacunador')->references('id')->on('vacunadores');
             /* datos de la institución*/
            $table->string('ips')->nullable();
            $table->string('departamento')->nullable();
            $table->string('municipio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulario_vacunas');
    }
}
