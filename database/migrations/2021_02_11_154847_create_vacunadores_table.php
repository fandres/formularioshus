<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacunadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacunadores', function (Blueprint $table) {
            $table->increments('id');
             /* datos vacunador*/
             $table->string('nombres_vacunador')->nullable();
             $table->string('apellidos_vacunador')->nullable();
             $table->char('tipocedula_vacunador',2)->nullable();
             $table->string('cedula_vacunador')->nullable();
             $table->string('lugar_expecedula_vacunador')->nullable();
             $table->string('firma_vacunador')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacunadores');
    }
}
