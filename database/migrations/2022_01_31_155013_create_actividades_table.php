<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->char('id_tipos',2);
            $table->date('fecha_solicitud');
            $table->date('fecha_atencion');
            $table->time('hora_atencion');
            $table->date('fecha_finalactividad');
            $table->char('id_mediosolicitud',2);
            $table->string('solicitante');
            $table->char('id_servicios',2);
            $table->char('id_lugares',2);
            $table->string('actividadsolicitada');
            $table->string('actividadrealizada');
            //1 = Finalizada 2 = pendiente 3 = Seguimiento
            $table->char('estadoactividad',2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividades');
    }
}
