<?php

namespace App\Http\Controllers\ActividadesInge;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ActividadesInge\Actividades;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
class ActividadesController extends Controller
{
    public function __construct()
    {   
        $this->middleware('auth');
        $this->middleware(['role:ingesoporte']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::id();
            return $next($request);
        });    
    }
    public function viewactividades()
    {
        $mediosolicitud = DB::table('actividades_mediosolicitud')->get();
        $lugares = DB::table('actividades_lugares')->get();
        $tipoactividad = DB::table('actividades_tipos')->get();
        $servicios = DB::table('capacitaciones_servicios')->get();
        return view('actividadesInge.viewactividades',compact('mediosolicitud','lugares','tipoactividad','servicios'));
    }
    public function validositienecargo()
    {
        $validacion = DB::table('actividades_asignarcargos as a')
            ->select('a.*','b.nombre_cargo')
            ->join('actividades_cargos as b','a.id_cargo','b.id')
            ->where('id_user',$this->user)->first();
        if($validacion)
        {
            return response()->json($validacion);
        }
        else{
            return 1;
        }        
    }
    public function obtenercargos()
    {
        $cargos = DB::table('actividades_cargos')->get();
        return response()->json($cargos);
    }
    public function updatecargoinge(Request $request)
    {
        //return $request->all();
        DB::table('actividades_asignarcargos')
        ->where('id_user', $this->user)
        ->update(['id_cargo' => $request->cargoparaasignar]);
    }
   
    public function asignarcargoinge(Request $request)
    { 
       // return $request->all();
        $inset = DB::table('actividades_asignarcargos')->insert([
            'id_user' => $this->user,
            'id_cargo' => $request->cargoparaasignar,
        ]);
    }
    public function createactividad(Request $request)
    {
        $rules =[
            'id_tipos'=>'required',
            'fecha_solicitud'=>'required',
            'fecha_atencion'=>'required',
            'hora_atencion'=>'required',
            'fecha_finalactividad'=>'required',
            'id_mediosolicitud'=>'required',
            'solicitante'=>'required', 
            'id_servicios'=>'required', 
            'id_lugares'=>'required', 
            'actividadsolicitada'=>'required', 
            'actividadrealizada'=>'required',
            'estadoactividad'=>'required' 
            ];
            $messages =[
                'id_tipos.required' => 'Seleccion el tipo de actividad',
                'fecha_solicitud.required' => 'El campo Fecha solicitud es requerido',
                'fecha_atencion.required' => 'El campo fecha atención es requerido',
                'hora_atencion.required' => 'El campo hora atención es requerido',
                'fecha_finalactividad.required' => 'El campo Fecha Final actividad es requerido',
                'id_mediosolicitud.required' => 'El campo medio solicitud requerido',
                'solicitante.required' => 'El campo solicitante es requerido',
                'id_servicios.required' => 'El campo servicio es requerido',
                'id_lugares.required' => 'El campo Lugar es requerido',
                'actividadsolicitada.required' => 'El campo actividad solicitada es requerido',
                'actividadrealizada.required' => 'El campo actividad realizada es requerido',
                'estadoactividad.required' => 'El campo estado actividad es requerido',
            ];
            $this->validate($request, $rules, $messages);

        $formacion = new Actividades($request->all());
        $formacion->id_user =$this->user;
        $formacion->save();
        return response()->json($formacion);
    }
    public function consultactividades()
    {
        $consulta = Actividades::actividadesrealiazadas()->where('a.id_user',$this->user)->orderby('a.id','DESC')->get();

        return response()->json($consulta);
    }
    public function editaractividad($id)
    {
        $actividades = Actividades::findOrFail($id);
        return response()->json($actividades);       
    }
    public function updateactividad(Request $request,$id)
    {     
        $actividades = Actividades::findOrFail($id);
        $actividades->update($request->all());
        return response()->json($actividades);
    }
    public function eliminaractividad($id)
    {
        $actividades = Actividades::findOrFail($id);
        $actividades->delete();
        if($actividades)
        {
            return 1;
        }
    }
}
