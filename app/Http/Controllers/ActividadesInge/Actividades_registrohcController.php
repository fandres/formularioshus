<?php

namespace App\Http\Controllers\ActividadesInge;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ActividadesInge\ActividadesRegistrohc;
use App\Http\Requests\Registrohc;
use Carbon\Carbon;
use DB;

class Actividades_registrohcController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware(['role:ingesoporte']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::id();
            return $next($request);
        });
    }
    public function viewregistrohc()
    {
        return view('actividadesInge.viewactividadesregistrohc');
    }
    public function registroshctable()
    {
        $registroshctabla = DB::table('actividades_registrohcs')
        ->orderBy('id','DESC')
        ->get();
        return response()->json($registroshctabla);
    }
    public function getcargoshc()
    {
        $putnbombre= DB::table('actividades_cargoshc')->get();
        return response()->json($putnbombre);
    }
    
    public function getsubgerencias()
    {
        $subgerenciashc= DB::table('actividades_subgerenteshc')->get();
        return response()->json($subgerenciashc);
    }
    public function getservicios()
    {
        $actividades_serviciohc= DB::table('actividades_serviciohc')->get();
        return response()->json($actividades_serviciohc);
    }
    public function createRegistroHc(Registrohc $request)
    {
        $registrohc = new ActividadesRegistrohc($request->all());
        $registrohc->id_user = $this->user;
        $registrohc->save();
        return response()->json($registrohc);
    }
    public function editarregistrohc($id)
    {
        $editarresgistrohc = ActividadesRegistrohc::findOrFail($id);
        return response()->json($editarresgistrohc);
    }
    public function updateregistrohc(Registrohc $request, $id)
    {
        $actualizando = ActividadesRegistrohc::findOrFail($id);
        $actualizando->update($request->all());
        return response()->json($actualizando);
    }
    public function eliminarregistrohc($id)
    {
        $eliminando = ActividadesRegistrohc::findOrFail($id);
        $eliminando->delete();
        if($eliminando)
        {
            return 1;
        }
    }
}
