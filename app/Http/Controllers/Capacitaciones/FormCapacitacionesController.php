<?php

namespace App\Http\Controllers\Capacitaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Capacitaciones\CapacitacionesDetalles;
use DB;
use Carbon\Carbon;

class FormCapacitacionesController extends Controller
{
    public function viewregistrarmecapacitacion()
    {
        return view('formCapacitaciones.viewpanelcodigo');
    }
    public function viewcapacitacion(Request $request)
    {   
        $codigo = $request->codigo;
        $empresas = DB::table('capacitaciones_empresas')->get();
        $servicios = DB::table('capacitaciones_servicios')->get();
        $cargos = DB::table('capacitaciones_cargos')->get();
        $cabecera = DB::table('capacitaciones_cabeceras')->where('codigo',$request->codigo)->first();
        if($cabecera)
        {
            $fechaC= $cabecera->fecha_capacitacion;
            $horafinC =  Carbon::parse($cabecera->horafinal)->addHour(1);                      
            $horaActual = now()->format('H:i');
            $horaActual =  Carbon::parse($horaActual);
            $horafinC = $horafinC->format('H:i');
            $horaActual = $horaActual->format('H:i');
            $fechaActual = now()->format('Y-m-d');
            //return '--horafinC--'.$horafinC.'<br>--horaActual--'.$horaActual;
            if($fechaActual == $fechaC and $horaActual < $horafinC )
            {
                return view('formCapacitaciones.formcapacitacion',compact('empresas','servicios','cargos','cabecera','codigo'));
            }
            else{
                return back()->with('mensaje', 'Supero el limite de tiempo para el registro');
            }
            
        }
        else{
            return back()->with('mensaje', 'No se encuentra capacitación con código: '.$codigo);
        }
       
        
    }
    public function viewcapacitacionurl($codigo)
    {  
        $empresas = DB::table('capacitaciones_empresas')->get();
        $servicios = DB::table('capacitaciones_servicios')->get();
        $cargos = DB::table('capacitaciones_cargos')->get();
        $cabecera = DB::table('capacitaciones_cabeceras')->where('codigo',$codigo)->first();
        if($cabecera)
        {
            return view('formCapacitaciones.formcapacitacion',compact('empresas','servicios','cargos','cabecera','codigo'));
        }
        else{
            return back()->with('mensaje', 'No se encuentra capacitación con código: '.$codigo);
        }
       
        
    }
    public function guardarcapacitacion(Request $request)
    {
        $request->validate([
            'nombres'=>'required',
            'fecha_entrega'=>'required',
            'apellidos'=>'required',
            'tipodocumento'=>'required',
            'documento'=>'required',
            'cargo'=>'required',
            'servicio'=>'required',
            'empresa'=>'required',           
            ]);
            // We create a variable to define the name of the file
            // Here it's the date and the extension signature.png
            // $folderPath = public_path('firmacarnets/');

            // // NOMBRES PARA GUARDAR LA IMAGEN EN LA CARPERTA Y BASE DE DATOS
            // $filename = date('mdYHis') . "-signature.png";
            // $uniAcudiente = uniqid();
            // $firma_acudiente= $uniAcudiente . "-signature.png";
            $data_uri = $request->signature;
            $encoded_image = explode(",", $data_uri)[1];
            // ALMACENAMIENTO EN BD
            $signature = new CapacitacionesDetalles();
            //$signature->firmaEntrega = $filename;
            $signature->capacitaciones_cabecera = $request->id_cabecera;// NO ES EL CODIGO PENDEJO ES EL ID DE LA CABECERA
            $signature->firmaEntrega = $encoded_image;
            $signature->fecha_entrega = $request->fecha_entrega;
            $signature->tipodocumento = $request->tipodocumento;
            $signature->documento = $request->documento;
            $signature->nombres = mb_strtoupper($request->nombres);
            $signature->apellidos = mb_strtoupper($request->apellidos);
            $signature->cargo = $request->cargo;
            $signature->servicio_detalle = $request->servicio;
            $signature->empresa= $request->empresa;         
            $signature->save();
            
            //FIRMA            
            
            // $data_uri = $request->signature;
            // $encoded_image = explode(",", $data_uri)[1];
            // $decoded_image = base64_decode($encoded_image);
            // file_put_contents($folderPath.$filename, $decoded_image);

            // Text of the alter to confirm that the data is posted
            return response()->json(['success'=>'Registro Exitoso']);
    }
}
