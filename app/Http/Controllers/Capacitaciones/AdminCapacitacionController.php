<?php

namespace App\Http\Controllers\Capacitaciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Capacitaciones\CapacitacionesCabeceras;
use App\Models\Capacitaciones\CapacitacionesDetalles;
use App\Exports\Capacitaciones\ExcelCapacitaciones;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;

class AdminCapacitacionController extends Controller
{
    
    public function __construct()
    {   $this->middleware('auth');   
        $this->middleware(['role:capacitador|ingesoporte']);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::id();
            return $next($request);
        });
    }
    public function viewcrearcapacitacion()
    {
      
       return view('formCapacitaciones.admin.crearcapacitacion');
     //  return view('formCapacitaciones.admin.admin');
    }
    
    public function crearcapacitacion(Request $request)
    {
            $rules =[
            'tema'=>'required',
            'responsable'=>'required',
            'fecha_capacitacion'=>'required',
            'servicio_cabecera'=>'required',
            'tipo'=>'required',
            'horainicial'=>'required',
            'horafinal'=>'required', 
            'tiporc'=>'required', 
            ];
            $messages =[
                'tema.required' => 'El campo Tema es requerido',
                'responsable.required' => 'El campo es requerido',
                'fecha_capacitacion.required' => 'El campo Fecha es requerido',
                'servicio_cabecera.required' => 'El campo Servicio es requerido',
                'tipo.required' => 'El campo Tipo es requerido',
                'horainicial.required' => 'El campo Hora inicial es requerido',
                'horafinal.required' => 'El campo Hora Final es requerido',
                'tiporc.required' => 'El campo capacitacion/reunion es requerido',
            ];
            $this->validate($request, $rules, $messages);
            //codigo
            $rand= rand(3,999);
            $date = Carbon::now();
            $date->year;
            $año = substr($date, 2, 2);
            $codigo= $año.$rand;
            
            $capacitacion = new CapacitacionesCabeceras();
            $capacitacion->id_user =  $this->user;
            $capacitacion->codigo = $codigo;
            $capacitacion->tema = $request->tema;
            $capacitacion->responsable = $request->responsable;
            $capacitacion->fecha_capacitacion = $request->fecha_capacitacion;
            $capacitacion->servicio_cabecera = $request->servicio_cabecera;
            $capacitacion->tipo = $request->tipo;
            $capacitacion->tiporc = $request->tiporc;
            $capacitacion->horainicial = $request->horainicial;
            $capacitacion->horafinal = $request->horafinal;
            $capacitacion->linkevaluacion = $request->linkevaluacion; 
            $capacitacion->save();
            return response()->json($capacitacion);
    }
    public function consultacapacitaciones()
    {
        $capacitaciones= DB::table('capacitaciones_cabeceras as a')
        ->select('a.id','a.tema','a.fecha_capacitacion','a.tipo','a.codigo','b.nombre_servicio')
        ->join('capacitaciones_servicios as b','a.servicio_cabecera','b.id')
        ->where('id_user',$this->user)
        ->orderBy('a.id','desc')
        ->get();
        if(count($capacitaciones)>=1)
        {        
          return response()->json($capacitaciones);
        }
        else{
            return 1;
        }
    }
    public function servicios()
    {
        $servicios = DB::table('capacitaciones_servicios')->get();
        return response()->json($servicios);
    }
    public function editarcapacitacion($id)
    {
        $capacitacion = CapacitacionesCabeceras::findOrFail($id);
        return response()->json($capacitacion);
    }
    public function updatecapacitacion(Request $request,$id)
    {
        $capacitacion = CapacitacionesCabeceras::findOrFail($id);
        $capacitacion->update($request->all());
    }
    public function eliminarcapacitacion($id)
    {
        $capacitacion = CapacitacionesCabeceras::findOrFail($id);
        $capacitacion->delete();
        if($capacitacion)
        {
            return 1;
        }
    }
    public function viewreportescapacitacion()
    {   
        $empresas = DB::table('capacitaciones_empresas')->get();
        $servicios = DB::table('capacitaciones_servicios')->get();
        $cargos = DB::table('capacitaciones_cargos')->get();
        return view('formCapacitaciones.admin.exportcapacitaciones',compact('empresas','servicios','cargos'));
    }
    public function viewmodaldetallecabecera($id)
    {
        $infomodal = CapacitacionesCabeceras::cabeceradetalle()->where('a.id',$id)->get();
        return response()->json($infomodal);
    }
    public function reportescapacitacion(Request $request)
    {
        return Excel::download(new ExcelCapacitaciones($request->fecha, $request->servicio,$request->cargo,$request->empresa), 'ProgramaEntregaCarnet.xlsx');

        // return 
        // $capacitacion = CapacitacionesDetalles::join('capacitaciones_cabeceras','capacitaciones_detalles.capacitaciones_cabecera','capacitaciones_cabeceras.id')
        //    ->join('capacitaciones_servicios','capacitaciones_detalles.servicio_detalle','capacitaciones_servicios.id')
        //    ->join('capacitaciones_empresas','capacitaciones_detalles.empresa','capacitaciones_empresas.id')
        //    ->join('capacitaciones_cargos','capacitaciones_detalles.cargo','capacitaciones_cargos.id')
        //   ->join('users','capacitaciones_cabeceras.id_user','users.id')
        //    ->selectRaw('capacitaciones_cabeceras.fecha_capacitacion,capacitaciones_cabeceras.tema,
        //    case capacitaciones_cabeceras.tipo
        //         when "P" then "Presencial" else "Virtual" end as tipo,
        //    case capacitaciones_cabeceras.tiporc
        //         when "C" then "Capacitacion" else "Reunión" end as tiporc,
        //     capacitaciones_cabeceras.horainicial,capacitaciones_cabeceras.horafinal,capacitaciones_servicios.nombre_servicio,users.name,
        //     CONCAT(capacitaciones_detalles.nombres," ",capacitaciones_detalles.apellidos) as nombres,capacitaciones_detalles.documento,
        //     capacitaciones_cargos.nombre_cargo,capacitaciones_empresas.nombre_empresa
        //     ')
        //    ->fecha($request->fecha)
        //    ->servicio($request->servicio)        
        //    ->cargo($request->cargo)
        //    ->empresa($request->empresa)
        //    ->get();
    }
    public function pruebas()
    {
        // return  $empleados = User::orderBy('id','DESC')
        // //->documento($request->documento)  
        // ->documento($request->documento)  
        // ->name($request->nombres)        
        // ->apellidos($request->apellidos)
        // ->select('apellidos')
        // ->get();
    }
}

