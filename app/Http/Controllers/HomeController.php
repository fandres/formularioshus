<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function adminredirect()
    {
       // Auth::user()
        if (Auth::user()->hasRole('capacitador') == 'capacitador') {
            return redirect('/viewcrearcapacitaciones');
        }
        elseif( Auth::user()->hasRole('carnets') == 'carnets'){
            return redirect('viewformcarnetentrega');
        }
        elseif( Auth::user()->hasRole('ingesoporte') == 'ingesoporte'){
            return redirect('viewactividades');
        }
        elseif( Auth::user()->hasRole('auditor') == 'consultor_capacitaciones'){
            return redirect('viewcrearcapacitaciones');
        }
    }
}
