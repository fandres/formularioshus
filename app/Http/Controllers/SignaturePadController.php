<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormularioVacuna;
use DB;
use PDF;

class SignaturePadController extends Controller
{
    public function index()
    {
        return view('signaturePad');
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function upload(Request $request)
    {
        $folderPath = public_path('upload/');
        
        $image_parts = explode(";base64,", $request->signed);
              
        $image_type_aux = explode("image/", $image_parts[0]);
           
        $image_type = $image_type_aux[1];
           
        $image_base64 = base64_decode($image_parts[1]);
        $uni = uniqid();
        $file = $folderPath . $uni . '.'.$image_type;
        file_put_contents($file, $image_base64);
        return back()->with('success', 'success Full upload signature');
    }
    
    public function guardar(Request $request)
    {
        $request->validate([
            'nombres'=>'required',
            'apellidos'=>'required',
            'tipocedula'=>'required',
            'cedula'=>'required',
            'fecha_nacimiento'=>'required',
            'edad'=>'required',
            'fecha_consentimiento'=>'required',
            'ciudad'=>'required',
            'eps'=>'required',
            'nombre_vacuna'=>'required',
            'dosis'=>'required',
            'acepta'=>'required',
            'id_vacunador' => 'required',
            
            ]);
            // We create a variable to define the name of the file
            // Here it's the date and the extension signature.png
            $folderPath = public_path('upload/');

            // NOMBRES PARA GUARDAR LA IMAGEN EN LA CARPERTA Y BASE DE DATOS
            $filename = date('mdYHis') . "-signature.png";
            $uniAcudiente = uniqid();
            $firma_acudiente= $uniAcudiente . "-signature.png";
        
        //VACUNADOR FIRMA
        //  $uniVacunador = uniqid();
        //  $firma_vacunador= $uniVacunador . "-signature.png";
        //  $data_uri_vacunador = $request->signaturePadVacunador;
        //  $encoded_image_Vacunador = explode(",", $data_uri_vacunador)[1];
        //  $decoded_image_vacunador = base64_decode($encoded_image_Vacunador);
        //  file_put_contents($folderPath.$firma_vacunador, $decoded_image_vacunador);
            
            // ALMACENAMIENTO EN BD
            $signature = new FormularioVacuna();
            $signature->firma_empleado = $filename;
            $signature->firma_acudiente = $firma_acudiente;
        // $signature->firma_vacunador = $firma_vacunador;
            $signature->tipocedula = $request->tipocedula;
            $signature->cedula = $request->cedula;
            $signature->nombres = mb_strtoupper($request->nombres);
            $signature->apellidos = mb_strtoupper($request->apellidos);
            $signature->fecha_nacimiento = $request->fecha_nacimiento;
            $signature->edad = $request->edad;
            $signature->fecha_consentimiento= $request->fecha_consentimiento;
            $signature->ciudad= mb_strtoupper($request->ciudad);
            $signature->eps= mb_strtoupper($request->eps);
            $signature->nombre_vacuna= mb_strtoupper($request->nombre_vacuna);
            $signature->dosis=$request->dosis;
            $signature->acepta=$request->acepta;
            $signature->razon_nofirma= $request->razon_nofirma;
            //ACUDIENTE
            $signature->nombres_acudiente= mb_strtoupper($request->nombres_acudiente);
            $signature->apellidos_acudiente= mb_strtoupper($request->apellidos_acudiente);
            $signature->tipocedula_acudiente= $request->tipocedula_acudiente;
            $signature->cedula_acudiente= $request->cedula_acudiente;
            $signature->lugar_expecedula_acudiente= mb_strtoupper($request->lugar_expecedula_acudiente);
            //VACUNADOR id_vacunador
            $signature->id_vacunador=$request->id_vacunador;
        //  $signature->nombres_vacunador= mb_strtoupper($request->nombres_vacunador);
        //  $signature->apellidos_vacunador= mb_strtoupper($request->apellidos_vacunador);
        //  $signature->tipocedula_vacunador= $request->tipocedula_vacunador;
        //  $signature->cedula_vacunador= $request->cedula_vacunador;
        //  $signature->lugar_expecedula_vacunador= mb_strtoupper($request->lugar_expecedula_vacunador);

            $signature->email = $request->email;
            $signature->save();
            
            //PACIENTE FIRMA
            
            
            $data_uri = $request->signature;
            $encoded_image = explode(",", $data_uri)[1];
            $decoded_image = base64_decode($encoded_image);
            file_put_contents($folderPath.$filename, $decoded_image);
        
        //ACUDIENTE FIRMA
        
        $data_uri_acudiente = $request->signaturePadAcudiente;
        $encoded_image_Acudiente = explode(",", $data_uri_acudiente)[1];
        $decoded_image_acudiente = base64_decode($encoded_image_Acudiente);
        file_put_contents($folderPath.$firma_acudiente, $decoded_image_acudiente);

            // Text of the alter to confirm that the data is posted
            return response()->json(['success'=>'Registro Exitoso']);

            
    }
    public function pdf($id)
    {
        $pdf = DB::table('formulario_vacunas as a')
        ->selectRaw('a.*,b.*,a.created_at as registro')
        ->join('vacunadores as b','a.id_vacunador','b.id')
        ->where('a.id',$id)
        ->first();
        $pdfnomber = $pdf->cedula;
        // return view('vacunacion.viewPdf',compact('pdf'));

        $pdf = PDF::loadView('vacunacion.viewPdf', ['pdf' => $pdf])->setPaper('a4', 'portrait');

        return $pdf->stream('husVacuna'.$pdfnomber. '.pdf');

    }
    
}
