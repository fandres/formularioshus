<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Exports\ExcelFormCarnetEntrega;
use App\Exports\ExcelFormInstitucion;
use App\Exports\ExcelFormPrograma;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\FormEntregaCarnet;

class FormAdminEntregaCarnetController extends Controller
{
    //constructor
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(['role:carnets']);
    }
    public function viewAdmin()
    {
        $instituciones = DB::table('instituciones')->get();
        $programas = DB::table('programas')->get();
        $perfiles = DB::table('perfiles')->get();
        return view('formEntregaCarnet.admin',compact('instituciones','programas','perfiles'));
    }
    public function excelformfecha(Request $request)
    {   
        if ($request->input('inicio') > $request->input('final')) {
            return back()->with('mensaje', 'La Fecha INICIAL no puede ser mayor que la fecha FINAL');
        } else {
           return Excel::download(new ExcelFormCarnetEntrega($request->inicio, $request->final), 'FormularioEntregaCarnet.xlsx');
         }
    }
    public function excelforminstitucion(Request $request)
    {
        //return $request->all();
        if ($request->input('inicio') > $request->input('final')) {
            return back()->with('mensaje', 'La Fecha INICIAL no puede ser mayor que la fecha FINAL');
        } else {
           return Excel::download(new ExcelFormInstitucion($request->inicio, $request->final,$request->institucion), 'InstitucionesEntregaCarnet.xlsx');
         }
    }
    public function excelformprograma(Request $request)
    {
        if ($request->input('inicio') > $request->input('final')) {
            return back()->with('mensaje', 'La Fecha INICIAL no puede ser mayor que la fecha FINAL');
        } else {
           return Excel::download(new ExcelFormPrograma($request->inicio, $request->final,$request->programa), 'ProgramaEntregaCarnet.xlsx');
         }
    }
    public function fichacarnets($id)
    {       
      $ficha = FormEntregaCarnet::fichaentrega()->where('a.id','=',$id)->first();
      // return response()->json($ficha);
       return view('formEntregaCarnet.fichaFirmas',compact('ficha'));
    }
}
