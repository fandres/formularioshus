<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormEntregaCarnet;
use App\Models\FormDevolucionCarnet;
use App\Models\FormReimpresionCarnet;
use DB;

class FormEntregaCarnetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');   
        $this->middleware(['role:carnets']);
    }
    public function viewEntrega()
    {
        $instituciones = DB::table('instituciones')->get();
        $programas = DB::table('programas')->get();
        $perfiles = DB::table('perfiles')->get();
        return view('formEntregaCarnet.entrega',compact('instituciones','programas','perfiles'));
    }
    public function viewDevolucion()
    {
        return view('formEntregaCarnet.ajax-devolucion');
    }
    public function viewReimpresion()
    {
        return view('formEntregaCarnet.ajax-reimpresion');
    }

    public function entrega(Request $request)
    {
        //return 'saf';
        $request->validate([
            'nombres'=>'required',
            'fecha_entrega'=>'required',
            'apellidos'=>'required',
            'tipodocumento'=>'required',
            'documento'=>'required',
            'perfil'=>'required',
            'programa'=>'required',
            'institucion'=>'required',           
            ]);
            // We create a variable to define the name of the file
            // Here it's the date and the extension signature.png
            // $folderPath = public_path('firmacarnets/');

            // // NOMBRES PARA GUARDAR LA IMAGEN EN LA CARPERTA Y BASE DE DATOS
            // $filename = date('mdYHis') . "-signature.png";
            // $uniAcudiente = uniqid();
            // $firma_acudiente= $uniAcudiente . "-signature.png";
            $data_uri = $request->signature;
            $encoded_image = explode(",", $data_uri)[1];
            // ALMACENAMIENTO EN BD
            $signature = new FormEntregaCarnet();
            //$signature->firmaEntrega = $filename;
            $signature->firmaEntrega = $encoded_image;
            $signature->fecha_entrega = $request->fecha_entrega;
            $signature->tipodocumento = $request->tipodocumento;
            $signature->documento = $request->documento;
            $signature->nombres = mb_strtoupper($request->nombres);
            $signature->apellidos = mb_strtoupper($request->apellidos);
            $signature->perfil = $request->perfil;
            $signature->programa = $request->programa;
            $signature->institucion= $request->institucion;            
            $signature->carnet=$request->carnet;
            $signature->cinta= $request->cinta;
            $signature->portacarnet= $request->portacarnet;            
            $signature->save();
            
            //FIRMA            
            
            // $data_uri = $request->signature;
            // $encoded_image = explode(",", $data_uri)[1];
            // $decoded_image = base64_decode($encoded_image);
            // file_put_contents($folderPath.$filename, $decoded_image);

            // Text of the alter to confirm that the data is posted
            return response()->json(['success'=>'Registro Exitoso']);
    }
    // public function devolucion()
    // {
    //     return view('formEntregaCarnet.devolucion');
    // }
    public function consultadevolver(Request $request)
    {
        $entregas= DB::table('form_entrega_carnets as a')
        ->selectRaw('a.*,b.fecha_reimpresion as rfechareimpresion,b.observacion,
            case b.carnet when 1 then "Entregado" else "" end as recarnet,
            case b.cinta when 1 then "Entregado" else "" end as recinta,
            case b.portacarnet when 1 then "Entregado" else "" end as reportacarnet
        ')            
        ->leftjoin('form_reimpresion_carnets as b','a.id','b.form_entrega_carnets')
        ->where('a.documento',$request->documento)
        ->where(function($query) use ($request) {
            $query
            ->orWhere('a.carnet',1)
            ->orWhere('a.cinta',1)
            ->orWhere('a.portacarnet',1);
        })
        ->first();

        if($entregas)
        {
            return response()->json($entregas);
        }
        else{
            return 1;
        }
    }
    public function devolviendoelementos(Request $request)
    {
            $data_uri = $request->signature;
            $encoded_image = explode(",", $data_uri)[1];
            // ALMACENAMIENTO EN BD
            $signature = new FormDevolucionCarnet();
            //$signature->firmaDevolucion = $filename;           
            $signature->firmaDevolucion = $encoded_image;           
            $signature->carnet=$request->carnet;
            $signature->cinta= $request->cinta;
            $signature->portacarnet= $request->portacarnet;
            $signature->form_entrega_carnets= $request->form_entrega_carnets; 
            $signature->fecha_devolucion= $request->fecha_devolucion;
            $signature->save();
            $request->carnet == 1 ? $request->carnet =2 : $request->carnet = 3;
            $request->cinta == 1 ? $request->cinta =2 : $request->cinta = 3;
            $request->portacarnet == 1 ? $request->portacarnet =2 : $request->portacarnet = 3;

            FormEntregaCarnet::where('id',$request->form_entrega_carnets)            
            ->update(['carnet'=>$request->carnet,'cinta'=>$request->cinta,'portacarnet'=>$request->portacarnet,]);
            return response()->json(['success'=>'Registro Exitoso']);
    }
    public function reimprimiendoelementos(Request $request)
    {
            $data_uri = $request->signature;
            $encoded_image = explode(",", $data_uri)[1];
            // ALMACENAMIENTO EN BD
            $signature = new FormReimpresionCarnet();
           // $signature->firmaReimpresion = $filename;           
            $signature->firmaReimpresion = $encoded_image;
            $signature->carnet=$request->carnet;
            $signature->cinta= $request->cinta;
            $signature->portacarnet= $request->portacarnet;
            $signature->observacion= $request->observacion;
            $signature->form_entrega_carnets= $request->form_entrega_carnets; 
            $signature->fecha_reimpresion= $request->fecha_reimpresion;            
            $signature->save();
            //FIRMA   
            // $data_uri = $request->signature;
            // $encoded_image = explode(",", $data_uri)[1];
            // $decoded_image = base64_decode($encoded_image);
            // file_put_contents($folderPath.$filename, $decoded_image);
            FormEntregaCarnet::where('id',$request->form_entrega_carnets)            
            ->update(['esreimpresion'=>1]);
            return response()->json(['success'=>'Registro Exitoso']);
    }
}
