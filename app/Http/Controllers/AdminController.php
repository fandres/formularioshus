<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vacunadore;
use DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->total = DB::table('formulario_vacunas')->count();
    }
    
    public function index(){
        //return 'hola';
        $contador=0;
        $total = $this->total;
        $fecha = date('Y-m-d');
        $formularios = DB::table('formulario_vacunas')
        ->whereDate('fecha_consentimiento',now())
        ->orderBy('created_at','desc')
        ->get();

        return view('vacunacion.adminIndex',compact('formularios','contador','fecha','total'));
    }
    public function vacunador()
    {
        return view('vacunacion.datosVacunador');
    }
    public function guardar(Request $request)
    {        
        $request->validate([
            
            'nombres_vacunador'=>'required',
            'apellidos_vacunador'=>'required',
            'tipocedula_vacunador'=>'required',
            'cedula_vacunador'=>'required',
            
          ]);
         // We create a variable to define the name of the file
         // Here it's the date and the extension signature.png
         $folderPath = public_path('vacunadores/');

         // We decode the image and store it in public folder
        
        //VACUNADOR
         $uniVacunador = uniqid();
         $firma_vacunador= $uniVacunador . "-signature.png";
         $data_uri_vacunador = $request->signaturePadVacunador;
         $encoded_image_Vacunador = explode(",", $data_uri_vacunador)[1];
         $decoded_image_vacunador = base64_decode($encoded_image_Vacunador);
         file_put_contents($folderPath.$firma_vacunador, $decoded_image_vacunador);
         
         // We store the signature file name in DB
         $signature = new Vacunadore();
         $signature->firma_vacunador = $firma_vacunador;
         //VACUNADOR
         $signature->nombres_vacunador= mb_strtoupper($request->nombres_vacunador);
         $signature->apellidos_vacunador= mb_strtoupper($request->apellidos_vacunador);
         $signature->tipocedula_vacunador= $request->tipocedula_vacunador;
         $signature->cedula_vacunador= $request->cedula_vacunador;
         $signature->lugar_expecedula_vacunador= mb_strtoupper($request->lugar_expecedula_vacunador);
         $signature->save();
         return response()->json(['success'=>'Registro Exitoso']);
         // Text of the alter to confirm that the data is posted
        

         
    }
    public function indexvacunador(){
      $vacunadores = DB::table('vacunadores')->get();
      $contador=0;
      return view('vacunacion.adminVacunadorIndex',compact('vacunadores','contador'));
    }
    public function buscarporfecha(Request $request)
    {   
       $fecha =$request->buscarfecha;
       $formularios = DB::table('formulario_vacunas')->whereDate('fecha_consentimiento',$fecha)->get();
       $contador=0;
       $total = $this->total;       
       return view('vacunacion.adminIndex',compact('formularios','contador','fecha','total'));
    }

}
