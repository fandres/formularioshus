<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Registrohc extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fechasolicitud' => 'required',
            'horasolicitud' => 'required',
            'id_subgerenciasolicitante' => 'required',
            'id_serviciosolicitante' => 'required',
            'solicitante' => 'required',
            'id_cargosolicitante' => 'required',
            'novedad' => 'required',
            'codigohc' => 'required',
            'nombrehc' => 'required',
            'fechaentrega' => 'required',
            'horaentrega' => 'required',
            'criteriosnocumplidos' => 'required',
            'criterioscumplidos' => 'required',
            'evaluacion' => 'required',
        ];
    }
    public function messages()
    {
        return[
            'fechasolicitud.required' => 'Fecha es requerida',
            'horasolicitud.required' => 'Hora requerida',
            'id_subgerenciasolicitante.required' => 'Subgerencia requeridad',
            'id_serviciosolicitante.required' => 'Campo requerido',
            'solicitante.required' => 'Solicitante requerido',
            'id_cargosolicitante.required' => 'Campo requerido',
            'novedad.required' => 'Novedad requerida',
            'codigohc.required' => 'Código requerido',
            'nombrehc.required' => 'Nombre requerido',
            'fechaentrega.required' => 'Fecha requerida',
            'horaentrega.required' => 'Hora requerida',
            'criteriosnocumplidos.required' => 'Campo requerido',
            'criterioscumplidos.required' => 'Campo requerido',
            'evaluacion.required' => 'Campo requerido',
        ];
    }
}
