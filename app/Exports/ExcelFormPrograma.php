<?php

namespace App\Exports;

//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;

//class ExcelFormCarnetEntrega implements FromCollection
class ExcelFormPrograma implements FromQuery, WithHeadings, WithTitle
{
    public function __construct($inicio,$final,$programa)
    {
          $this->inicio = $inicio;
          $this->final = $final;
          $this->programa = $programa;
    }
   use Exportable;
   public function headings(): array
   {
       return [
        'Fecha Entrega',
           'Nombres',
           'Apellidos',
           'Documento',
           'Perfil',
           'Programa',
           'Intitución',
           'Carnet',
           'Cinta',
           'Porta Carnet',
           'Fecha Devolución',
           'Reimpresión',
           'Fecha Reimpresión',
           'Carnet Reimpresióm',
           'Cinta Reimpresióm',
           'Porta Reimpresióm',
           'Observación Reimpresióm',
           'Firmas'          
       ];
   }
    public function query()
    {           
    return DB::table('form_entrega_carnets as a')
    ->selectRaw('
    a.fecha_entrega,a.nombres,a.apellidos,a.documento,
        f.nombre as perfil,d.nombre as programa,e.nombre as institucion,
        case a.carnet when null then "No" when 1 then "Entregado" when 2 then "Devuelto" when 3 then "No Devuelto" end as Carnet,
        case a.cinta when null then "No" when 1 then "Entregado" when 2 then "Devuelto" when 3 then "No Devuelto" end as Cinta,
        case a.portacarnet when null then "No" when 1 then "Entregado" when 2 then "Devuelto" when 3 then "No Devuelto" end as Porta_carnet,    
        b.fecha_devolucion,
        case a.esreimpresion when 1 then "Entregado" else "" end as reimpresion,
        c.fecha_reimpresion,
        case c.carnet when 1 then "Entregado" else "" end as reimpresioncarnet,
        case c.cinta when 1 then "Entregado" else "" end as reimpresioncinta,
        case c.portacarnet when 1 then "Entregado" else "" end as reimpresionportacarnet,c.observacion,
        concat("http://127.0.0.1:8000/fichafirmascarnet/",a.id)
    ')
    ->leftJoin('form_devolucion_carnets as b','a.id','b.form_entrega_carnets')
    ->leftJoin('form_reimpresion_carnets as c','a.id','c.form_entrega_carnets')
    ->join('programas as d','a.programa','d.id')
    ->join('instituciones as e','a.institucion','e.id')
    ->join('perfiles as f','a.perfil','f.id')
    ->whereBetween('a.fecha_entrega',[$this->inicio,$this->final])
    ->where('a.programa','=',$this->programa)
    ->orderBy('a.fecha_entrega','ASC');
    
    //return $juntas;
    }
    public function title(): string
    {
        return 'EntregaCarnet';
    }
}
