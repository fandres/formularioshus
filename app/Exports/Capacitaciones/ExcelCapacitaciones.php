<?php

namespace App\Exports\Capacitaciones;

//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;
use App\Models\Capacitaciones\CapacitacionesCabeceras;
use App\Models\Capacitaciones\CapacitacionesDetalles;

//class ExcelFormCarnetEntrega implements FromCollection
class ExcelCapacitaciones implements FromQuery, WithHeadings, WithTitle
{
    public function __construct($fecha,$servicio,$cargo,$empresa)
    {
          $this->fecha = $fecha;
          $this->servicio = $servicio;
          $this->cargo = $cargo;
          $this->empresa = $empresa;
    }
   use Exportable;
   public function headings(): array
   {
       return        
       [
    //     ['TITULO'],
    //         [
    //         'Fecha Entrega',
    //         'Nombres',
    //         'Apellidos',
    //         'Documento',
    //         'Perfil',
    //         'Programa',
    //         'Intitución',
    //         'Carnet',
    //         'Cinta',
    //         'Porta Carnet',
    //         'Fecha Devolución',
    //         'Reimpresión',
    //         'Fecha Reimpresión',
    //         'Carnet Reimpresióm',
    //         'Cinta Reimpresióm',
    //         'Porta Reimpresióm',
    //         'Observación Reimpresióm',
    //         'Firmas'
    //         ]
    //    ];
                
            'Fecha',
            'Tema',
            'Forma',
            'Tipo',
            'Hora Inicio',
            'Hora Final',
            'Servicio',
            'Capacitador',
            'Nombre(s) y apellido',
            'Cédula',
            'Cargo',
            'Empresa',            
       ];
   }
    public function query()
    {           
        return 
        $capacitacion = CapacitacionesDetalles::join('capacitaciones_cabeceras','capacitaciones_detalles.capacitaciones_cabecera','capacitaciones_cabeceras.id')
           ->join('capacitaciones_servicios','capacitaciones_detalles.servicio_detalle','capacitaciones_servicios.id')
           ->join('capacitaciones_empresas','capacitaciones_detalles.empresa','capacitaciones_empresas.id')
           ->join('capacitaciones_cargos','capacitaciones_detalles.cargo','capacitaciones_cargos.id')
           ->join('users','capacitaciones_cabeceras.id_user','users.id')
           ->selectRaw('capacitaciones_cabeceras.fecha_capacitacion,capacitaciones_cabeceras.tema,
           case capacitaciones_cabeceras.tipo
                when "P" then "Presencial" else "Virtual" end as tipo,
           case capacitaciones_cabeceras.tiporc
                when "C" then "Capacitacion" else "Reunión" end as tiporc,
            capacitaciones_cabeceras.horainicial,capacitaciones_cabeceras.horafinal,capacitaciones_servicios.nombre_servicio,users.name,
            CONCAT(capacitaciones_detalles.nombres," ",capacitaciones_detalles.apellidos) as nombres,capacitaciones_detalles.documento,
            capacitaciones_cargos.nombre_cargo,capacitaciones_empresas.nombre_empresa
            ')
           ->fecha($this->fecha)
           ->servicio($this->servicio)        
           ->cargo($this->cargo)
           ->empresa($this->empresa)
           ->orderBy('capacitaciones_cabeceras.fecha_capacitacion','ASC');
    
    //return $juntas;
    }
    public function title(): string
    {
        return 'EntregaCarnet';
    }
}
