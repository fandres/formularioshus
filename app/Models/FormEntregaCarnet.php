<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class FormEntregaCarnet extends Model
{
    use HasFactory;

    public static function fichaentrega()
    {
        return DB::table('form_entrega_carnets as a')
        ->selectRaw('
        a.fecha_entrega,a.nombres,a.apellidos,a.documento,
        f.nombre as perfil,d.nombre as programa,e.nombre as institucion,
        case a.carnet when null then "No" when 1 then "Entregado" when 2 then "Devuelto" when 3 then "No Devuelto" end as Carnet,
        case a.cinta when null then "No" when 1 then "Entregado" when 2 then "Devuelto" when 3 then "No Devuelto" end as Cinta,
        case a.portacarnet when null then "No" when 1 then "Entregado" when 2 then "Devuelto" when 3 then "No Devuelto" end as Porta_carnet,    
        b.fecha_devolucion,
        case a.esreimpresion when 1 then "Entregado" else "" end as reimpresion,
        c.fecha_reimpresion,
        case c.carnet when 1 then "Entregado" else "" end as reimpresioncarnet,
        case c.cinta when 1 then "Entregado" else "" end as reimpresioncinta,
        case c.portacarnet when 1 then "Entregado" else "" end as reimpresionportacarnet,c.observacion,        
        a.firmaEntrega,b.firmaDevolucion,c.firmaReimpresion')
    ->leftJoin('form_devolucion_carnets as b','a.id','b.form_entrega_carnets')
    ->leftJoin('form_reimpresion_carnets as c','a.id','c.form_entrega_carnets')
    ->join('programas as d','a.programa','d.id')
    ->join('instituciones as e','a.institucion','e.id')
    ->join('perfiles as f','a.perfil','f.id');
    }

}

