<?php

namespace App\Models\Capacitaciones;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class CapacitacionesCabeceras extends Model
{
    use HasFactory;
    protected $fillable = [
        'fecha_capacitacion',
        'horafinal',
        'horainicial',        
        'servicio_cabecera',
        'tema',
        'responsable',
        'tipo',
        'tiporc',
        'linkevaluacion',
    ];
    public static function cabeceradetalle()
    {
        return DB::table('capacitaciones_cabeceras as a')
            ->join('capacitaciones_detalles as b','a.id','b.capacitaciones_cabecera')
            ->join('capacitaciones_cargos as c','b.cargo','c.id')
            ->join('capacitaciones_servicios as d','b.servicio_detalle','d.id')
            ->join('capacitaciones_empresas as e','b.empresa','e.id');

    }
    
}
