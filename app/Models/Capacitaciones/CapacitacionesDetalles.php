<?php

namespace App\Models\Capacitaciones;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CapacitacionesDetalles extends Model
{
    use HasFactory;

    public function scopeFecha($query, $fecha)
    {   
        if($fecha)
        return $query->where('fecha_capacitacion','>=',$fecha);
    }
    public function scopeServicio($query, $servicio)
    {   
        if($servicio)
        return $query->where('servicio_detalle',$servicio);
    }
    public function scopeEmpresa($query, $empresa)
    {   
        if($empresa)
        return $query->where('empresa',$empresa);
    }
    public function scopeCargo($query, $cargo)
    {   
        if($cargo)
        return $query->where('cargo',$cargo);
    }
}
