<?php

namespace App\Models\ActividadesInge;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActividadesRegistrohc extends Model
{
    use HasFactory;
    protected $fillable=[
        'fechasolicitud',
        'horasolicitud',
        'id_subgerenciasolicitante',
        'id_serviciosolicitante',
        'solicitante',
        'id_cargosolicitante',
        'novedad',
        'codigohc',
        'nombrehc',
        'fechaentrega',
        'horaentrega',
        'criteriosnocumplidos',
        'criterioscumplidos',
        'evaluacion',
        'estado',
    ];
}
