<?php

namespace App\Models\ActividadesInge;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Actividades extends Model
{
    use HasFactory;

    protected $fillable=[
        'id_tipos',
        'fecha_solicitud',
        'fecha_atencion',
        'hora_atencion',
        'fecha_finalactividad',
        'id_mediosolicitud',
        'solicitante',
        'id_servicios',
        'id_lugares',
        'actividadsolicitada',
        'actividadrealizada',
        'estadoactividad',
    ];
    public static function actividadesrealiazadas()
    {
        //1 = Finalizada 2 = pendiente 3 = Seguimiento
      return  DB::table('actividades as a')
            ->selectRaw('a.id,a.fecha_solicitud,a.solicitante,a.actividadsolicitada,estadoactividad
            
            ');
    }
}
